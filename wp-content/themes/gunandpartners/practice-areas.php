<?php
/**
/*
Template Name: Practice Areas
 * @package WordPress
 * @subpackage Gun + Partners
 * @since Gun + Partners 0.1
 */

get_header(); ?>
<?php get_sidebar(); ?>

<div id="main-content" class="main-content">

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post(); ?>

				<div class="row">
          <div class="col-8 col-sm-8 col-lg-8 col-xs-12">
            <h1 class="pageTitle"><?php the_title(); ?></h1>
            </div>
<?php 
if ( has_post_thumbnail() ) : ?>

<?php  // check if the post has a Post Thumbnail assigned to it.
        $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'header' );
  //the_post_thumbnail('header', array('class' => 'img-responsive scale'));
 
?>	
	<div class="col-12 col-sm-12 col-lg-12 col-xs-12 headerImage" style="
	background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(0,0,0,0) 59%, rgba(0,0,0,0.65) 100%), url(<?php echo $thumb['0'];?>) no-repeat center center; 
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(59%,rgba(0,0,0,0)), color-stop(100%,rgba(0,0,0,0.65))), url(<?php echo $thumb['0'];?>) no-repeat center center; 
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 59%,rgba(0,0,0,0.65) 100%), url(<?php echo $thumb['0'];?>) no-repeat center center; 
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 59%,rgba(0,0,0,0.65) 100%), url(<?php echo $thumb['0'];?>) no-repeat center center;  
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 59%,rgba(0,0,0,0.65) 100%), url(<?php echo $thumb['0'];?>) no-repeat center center; 
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 59%,rgba(0,0,0,0.65) 100%), url(<?php echo $thumb['0'];?>) no-repeat center center; 
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#a6000000',GradientType=0 ), url(<?php echo $thumb['0'];?>') no-repeat center center; 
  -webkit-background-size: 30px 44px, cover, cover;
  -moz-background-size: 30px 44px, cover, cover;
  -o-background-size: 30px 44px, cover, cover;
  background-size: 30px 44px, cover, cover;
  ">

	</div>
	    <?php endif ?>
</div>
				
				  <div class="row">
  
             <div class="col-8 col-sm-12 col-md-9 col-lg-9 col-xs-12 practiceareas">
<?php the_content(); ?>

            
            </div><!--/span-->
             <div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xs-3 hidden-xs hidden-sm practice">
               <!-- <div data-spy="affix">--><!--data-offset-top="60" data-offset-bottom="0"--> 
                  <div data-spy="affix" data-offset-top="422">
                <ul class="nav sidebar">
                  <li><a href="#corporate"><?php _e('Corporate and M&A','gunandpartners'); ?></a></li>
                  <li><a href="#finance"><?php _e('Finance','gunandpartners'); ?></a></li>
                  <li><a href="#insurance"><?php _e('Insurance and Reinsurance','gunandpartners'); ?></a></li>
                  <li><a href="#dispute"><?php _e('Dispute Resolution','gunandpartners'); ?></a></li>
                  <li><a href="#employment"><?php _e('Employment','gunandpartners'); ?></a></li>
                  <li><a href="#business"><?php _e('Business Crimes and Anti-Corruption','gunandpartners'); ?></a></li>
                  <li><a href="#tax"><?php _e('Administrative, Tax and Regulatory','gunandpartners'); ?></a></li>
                  <li><a href="#competition"><?php _e('Competition','gunandpartners'); ?></a></li>
                  <li><a href="#life"><?php _e('Life Sciences','gunandpartners'); ?></a></li>
                  <li><a href="#energy"><?php _e('Energy and Natural Resources','gunandpartners'); ?></a></li>
                  <li><a href="#technology"><?php _e('Technology, Media and Telecom','gunandpartners'); ?></a></li>
                  <li><a href="#construction"><?php _e('Construction and Real Estate','gunandpartners'); ?></a></li>
                  <li><a href="#intellectual"><?php _e('Intellectual Property Services','gunandpartners'); ?></a>
                  <ul class="nav">
                    
                  <li><a href="#patents"><?php _e('Patents and Utility Models','gunandpartners'); ?></a></li>
                  <li><a href="#trademarks"><?php _e('Trademarks and Industrial Designs','gunandpartners'); ?></a></li>
                  <li><a href="#copyrights"><?php _e('Copyrights','gunandpartners'); ?></a></li>
                  <li><a href="#investigation"><?php _e('Anti-Counterfeiting','gunandpartners'); ?></a></li>
                  <li><a href="#ip"><?php _e('IP Prosecution','gunandpartners'); ?></a></li>
                  </ul>
                  
                  </li>
                  <li><a href="#top"><span class="glyphicon glyphicon-chevron-up"></span> <?php _e('Back to the top','gunandpartners'); ?></a></li>
                </ul>
         </div>
           <!-- </div> -->

        </div><!--/span-->

        </div><!--/span-->
				
					<!--the_content();

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) {
						comments_template();
					}-->
					
				<?php endwhile;
			?>

		</div><!-- #content -->
	</div><!-- #primary -->
	
</div><!-- #main-content -->


<?php

get_footer(); ?>
