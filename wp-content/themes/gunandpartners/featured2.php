<?php
/**
/*
Template Name: Mevzuat
 * @package WordPress
 * @subpackage Gun + Partners
 * @since Gun + Partners 0.1
 */

get_header(); ?>
<?php get_sidebar(); ?>

<div id="main-content" class="main-content">

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

                    <?php
                             // $args = my_search_args();
                             // $my_search = new WP_Advanced_Search($args);
                            //  $temp_query = $wp_query;
                              //$wp_query = $my_search->query();
                    ?>
		    <div class="row">
<div class="col-8 col-sm-8 col-lg-8 col-xs-12">

            </div>
<?php 
$post->ID = 1233;
if ( has_post_thumbnail() ) : ?>

<?php  // check if the post has a Post Thumbnail assigned to it.
        $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'header' );
  //the_post_thumbnail('header', array('class' => 'img-responsive scale'));
 
?>	
	<div class="col-12 col-sm-12 col-lg-12 col-xs-12 headerImage" style="
	background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(0,0,0,0) 59%, rgba(0,0,0,0.65) 100%), url(<?php echo $thumb['0'];?>) no-repeat center center; 
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(59%,rgba(0,0,0,0)), color-stop(100%,rgba(0,0,0,0.65))), url(<?php echo $thumb['0'];?>) no-repeat center center; 
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 59%,rgba(0,0,0,0.65) 100%), url(<?php echo $thumb['0'];?>) no-repeat center center; 
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 59%,rgba(0,0,0,0.65) 100%), url(<?php echo $thumb['0'];?>) no-repeat center center;  
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 59%,rgba(0,0,0,0.65) 100%), url(<?php echo $thumb['0'];?>) no-repeat center center; 
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 59%,rgba(0,0,0,0.65) 100%), url(<?php echo $thumb['0'];?>) no-repeat center center; 
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#a6000000',GradientType=0 ), url(<?php echo $thumb['0'];?>') no-repeat center center; 
  -webkit-background-size: 30px 44px, cover, cover;
  -moz-background-size: 30px 44px, cover, cover;
  -o-background-size: 30px 44px, cover, cover;
  background-size: 30px 44px, cover, cover;
  ">

	</div>
	    <?php endif ?>
	    
</div>
		    <div class="row">
                     <div class="col-10 col-sm-12 col-lg-10 col-xs-12">
		    <br/>

		    <div class="panel-group" id="custom-collapse-0">
			<?php

				$categories = get_categories('order=ASC&child_of='.icl_object_id(16,'category',true).'&hide_empty=0');
				foreach ($categories as $key => $value) {
					$posts = get_posts(array('category' => $value->term_id));
					$pstring = "";
					foreach ($posts as $key1 => $post) {
						//echo wp_get_attachment_url(get_field('pdf_version'));
						$pdf_file = get_field('pdf_version');
						$pdffilesize = filesize( get_attached_file( $pdf_file ) );
						$pdffilesize = size_format($pdffilesize, 0);
						$word_file = get_field('word_version');
						$wordfilesize = filesize( get_attached_file( $word_file ) );
						$wordfilesize = size_format($wordfilesize, 0);
						$pstring .= "<div class='col-xs-8'><p>".$post->post_title."</p></div>";
						if(!empty($pdf_file)){
							$pstring .= '<div class="col-xs-2"><a href="'.wp_get_attachment_url( $pdf_file ).'"><button type="button" style="float:right; " class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="" data-original-title=".pdf - '.$pdffilesize.'"><span class="glyphicon glyphicon-file" style="color: #00ABDF;"></span> PDF </button></a></div>';
						}
						if(!empty($word_file)){
							$pstring .= '<div class="col-xs-2"><a href="'.wp_get_attachment_url( $word_file ).'"><button type="button" style="float:right; " class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="" data-original-title=".doc - '.$wordfilesize.'"><span class="glyphicon glyphicon-file" style="color: #00ABDF;"></span> WORD </button></a></div>';
						}
						

					}
					$string = "";
					$string .= '<div class="panel panel-default">';
					$string .= '<div class="panel-heading">';
					$string .= '<h4 class="panel-title">';
					$string .= '<a class="accordion-toggle" data-toggle="collapse" data-parent="#custom-collapse-0" href="#custom-collapse-'.$value->slug.$value->id.'">'.$value->name.'</a>';
					$string .= '</h4>';
					$string .= '</div>';
					$string .= '<div id="custom-collapse-'.$value->slug.$value->id.'" class="panel-collapse collapse">';
					$string .= '<div class="panel-body">';
					$string .= '<p>'.$pstring.'<br/><br/><br/><br/><br/></p>';
					$string .= '</div>';
					$string .= '</div>';
					$string .= '</div>';
					echo $string;

				}
			?>
			</div>
			
			<?php
				wp_reset_query();
			?>

		</div><!-- #content -->
	</div><!-- #primary -->
	
</div><!-- #main-content -->


<?php

get_footer(); ?>
