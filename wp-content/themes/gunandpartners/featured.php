<?php
/**
/*
Template Name: Featured
 * @package WordPress
 * @subpackage Gun + Partners
 * @since Gun + Partners 0.1
 */

get_header(); ?>
<?php get_sidebar(); ?>

<div id="main-content" class="main-content">

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

                    <?php
                             $args = my_search_args();
                             $my_search = new WP_Advanced_Search($args);
                             $temp_query = $wp_query;
                             $wp_query = $my_search->query();
                    ?>
		    <div class="row">
<div class="col-8 col-sm-8 col-lg-8 col-xs-12">
            </div>
<?php 
$post->ID = 953;
if ( has_post_thumbnail() ) : ?>

<?php  // check if the post has a Post Thumbnail assigned to it

        $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'header' );
  //the_post_thumbnail('header', array('class' => 'img-responsive scale'));
 
?>	
	<div class="col-12 col-sm-12 col-lg-12 col-xs-12 headerImage" style="
	background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(0,0,0,0) 59%, rgba(0,0,0,0.65) 100%), url(<?php echo $thumb['0'];?>) no-repeat center center; 
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(59%,rgba(0,0,0,0)), color-stop(100%,rgba(0,0,0,0.65))), url(<?php echo $thumb['0'];?>) no-repeat center center; 
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 59%,rgba(0,0,0,0.65) 100%), url(<?php echo $thumb['0'];?>) no-repeat center center; 
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 59%,rgba(0,0,0,0.65) 100%), url(<?php echo $thumb['0'];?>) no-repeat center center;  
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 59%,rgba(0,0,0,0.65) 100%), url(<?php echo $thumb['0'];?>) no-repeat center center; 
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 59%,rgba(0,0,0,0.65) 100%), url(<?php echo $thumb['0'];?>) no-repeat center center; 
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#a6000000',GradientType=0 ), url(<?php echo $thumb['0'];?>') no-repeat center center; 
  -webkit-background-size: 30px 44px, cover, cover;
  -moz-background-size: 30px 44px, cover, cover;
  -o-background-size: 30px 44px, cover, cover;
  background-size: 30px 44px, cover, cover;
  ">

	</div>
	    <?php endif ?>
	    
</div>
		    <div class="row">
                     <div class="col-10 col-sm-12 col-lg-10 col-xs-12">
		    <br/>

		    <hr>
			<?php
			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			$args = array( 'post_type' => 'post', 'posts_per_page' => 6,'paged' => $paged, 'meta_query' => array(
       					array(
           					'key' => 'featured_article',
							'value' => '1',
							'compare' => '=='
       					)
   					));
			$loop = new WP_Query( $args );
			$iter = 1; //iterator to insert clearfixes
			?>
		<?php while ( $loop->have_posts() ) : $loop->the_post();?>



			
		<div class="row">
                     <div class="col-xs-12">	
			
			
<article>
		<div class="row">
			<div class="col-xs-3 hidden-xs">
				<?php if ( has_post_thumbnail() ) : ?>
				<a href="<?php the_permalink() ?>"> <?php the_post_thumbnail('medium', array('class' => 'img-thumbnail')); ?></a>
				<?php else : ?>
				<a href="<?php the_permalink() ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/searchDefault.png" class="img-thumbnail" style="opacity: 0.75"></a>
				<?php endif ?>
			</div>
			<div class="col-sm-9 col-xs-12">
				<div class="row">
			<div class="col-xs-8">
		
				<h3 style="margin-bottom: 0px; margin-top: 0px;"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
				</div>
			<div class="col-xs-4">
				<?php

  $pdf_file = get_field('pdf_version');
  $word_file = get_field('word_version');

  if( !empty($pdf_file) ): ?>
  <?php $pdf_file = get_field('pdf_version');

 
$url = wp_get_attachment_url( $pdf_file );


$title = get_the_title( $pdf_file );


// part where to get the filesize
$filesize = filesize( get_attached_file( $pdf_file ) );
$filesize = size_format($filesize, 0);

?>
    <a href="<?php echo $url;?>">
    	<button type="button" style="float:right; "class="btn btn-default"  data-toggle="tooltip" data-placement="bottom" title=".pdf - <?php echo $filesize; ?>" >
  			<span class="glyphicon glyphicon-file" style="color: #00ABDF;"></span> PDF 
  		</button>
	</a>



   <?php endif ?>
				</div>
				


			<div class="col-xs-4">
				<?php

  $word_file = get_field('word_version');

  if( !empty($word_file) ): ?>
  <?php $word_file = get_field('word_version');
 
$url = wp_get_attachment_url( $word_file );
$title = get_the_title( $word_file );


// part where to get the filesize
$filesize = filesize( get_attached_file( $word_file ) );
$filesize = size_format($filesize, 0);

?>
    <a href="<?php echo $url;?>">
    	<button type="button" style="float:right; "class="btn btn-default"  data-toggle="tooltip" data-placement="bottom" title=".doc - <?php echo $filesize; ?>" >
  			<span class="glyphicon glyphicon-file" style="color: #00ABDF;"></span> WORD 
  		</button>
	</a>



   <?php endif ?>
				</div>
				</div>
			
		
			
		
                <p><small><?php
$posttags = get_the_tags();
if ($posttags) {
  foreach($posttags as $tag) {
    echo $tag->name . ' - '; 
  }
}?><time datetime="<?php the_time( 'd.m.Y' ); ?>"><?php the_time( 'd.m.Y' ); ?></time><br/> <?php

$categories = get_the_category();
$separator = ' | ';
$output = '';
$i=0;
if($categories){
	foreach($categories as $category) {
		if ($i==2) break;
		$output .= ''.$category->cat_name.''.$separator;
		$i++;
	}
echo trim($output, $separator);
}


?> || <?php if ( function_exists( 'coauthors_posts_links' ) ) {
    coauthors();
} else {
    the_author_posts_link();
} ?></small></p>
		
                 <p class="byline"><?php the_advanced_excerpt('length=12&length_type=words&no_custom=1&finish=sentence&exclude_tags=img,p,strong,h1,h2,h3,h4,h5&read_more=%26#187;'); ?></p>

		
                                    </div></div><hr></div>
</div>
               </article>
					
				<?php endwhile;
                $my_search->pagination(array('prev_text' => '&#171;','next_text' => '&#187;'));           
                                
			//else :

			//	echo 'Sorry, no posts matched your criteria.';

			//endif;
			
			//$wp_query = $temp_query;
			wp_reset_query();

			?>

		</div><!-- #content -->
	</div><!-- #primary -->
	
</div><!-- #main-content -->


<?php

get_footer(); ?>
