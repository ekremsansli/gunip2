<?php
/**
/*
Template Name: Knowledge & Wisdom
 * @package WordPress
 * @subpackage Gun + Partners
 * @since Gun + Partners 0.1
 */

get_header(); ?>
<?php get_sidebar(); ?>

<div id="main-content" class="main-content">

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<div class="row">
				<div class="col-sm-10 col-xs-12">
				<h1 class="pageTitle"><?php the_title(); ?></h1>
				<h3 style="text-transform: uppercase; margin-top: 0px;  font-weight: 100; color: #666666;"><?php _e('Featured Articles','gunandpartners'); ?></h3>
				</div>
             

<div class="col-12 col-sm-12 col-lg-12 col-xs-12">
<?php 
 
$image = get_field('header_image');
 
if( !empty($image) ): ?>
 
	<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="img-responsive"/>
 
<?php endif; ?>
 
</div>
</div>
				
				  <div class="row featured_story">
  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
      
	<div class="carousel-inner">
		<?php
			$args = array( 'post_type' => 'post', 'posts_per_page' => 5, 'meta_query' => array(
       array(
           'key' => 'featured_article',
			'value' => '1',
			'compare' => '=='
       )
   )  );
			$loop = new WP_Query( $args );
			$iter = 1; //iterator to insert clearfixes
			?>
		<?php while ( $loop->have_posts() ) : $loop->the_post();?>
		
		<div class="item <?php 
				if( $iter == 1 ): ?>
				active
				<?php endif; ?>
				">
		   <div class="col-md-4 col-xs-12">
			<figure>
				<?php  
if ( has_post_thumbnail() ) : ?>
<?php  $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'header' ); ?>
<a href="<?php the_permalink() ?>"><div class="col-12 col-sm-12 col-lg-12 col-xs-12 img-responsive" style="min-height: 262px;
	background: url(<?php echo $thumb['0'];?>) no-repeat center center;
  -webkit-background-size: cover, cover;
  -moz-background-size: cover, cover;
  -o-background-size: cover, cover;
  background-size: cover, cover;
  ">

	</div></a>

			
	<?php else : ?>
	   
				<a href="<?php the_permalink() ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/knowledge_1.jpg" class="img-responsive featured" width="300" height="300"></a>
			</figure>
			 <?php endif ?>
		</div>
		<div class="col-md-8 col-xs-12">
			<h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
			<p class="byline"><strong><?php
$posttags = get_the_tags();
if ($posttags) {
  foreach($posttags as $tag) {
    echo $tag->name . ' | '; 
  }
}
?></strong>
<?php

$categories = get_the_category();
$separator = ' | ';
$output = '';
$i=0;
if($categories){
	foreach($categories as $category) {
		if ($i==2) break;
		$output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator;
		$i++;
	}
echo trim($output, $separator);
}


?><br/><?php if ( function_exists( 'coauthors_posts_links' ) ) {
    coauthors();
} else {
    the_author_posts_link();
} ?></p>
			<p class="hidden-xs"><?php the_advanced_excerpt('length=12&length_type=words&no_custom=1&finish=sentence&exclude_tags=b,p,img,strong,h1,h2,h3,h4,h5&read_more=%26#187;'); ?></p>
			</div>
		</div>
		<?php $iter++; ?>
		<?php endwhile; ?>
        
      </div>
	<?php ///echo $iter; ?>
      <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
      </a>
      <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
      </a>
      <ol class="carousel-indicators">
	<?php
			$indicator = 0; //iterator to insert clearfixes
			?>
	<?php while ( $indicator != $iter-1 ) :?>
        <li data-target="#carousel-example-generic" data-slide-to="<?php echo $indicator; ?>" class=" <?php 
				if( $indicator == 0 ): ?>
				active
				<?php endif; ?>"></li>    
	<?php $indicator++; ?>
	<?php endwhile; ?>
      </ol>
    </div>
   </div>
	      <div class="row">
		<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xs-12">
	     
		
		 <div class="row">
             <div class="col-9 col-sm-12 col-md-9 col-lg-9 col-xs-12">
              <h3><?php _e('Latest Insights','gunandpartners'); ?></h3>
              <hr>
		 
		 <?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		 
			$args = array( 'post_type' => 'post', 'paged' => $paged, 'posts_per_page' => 5, 'meta_query' => array(
       array(
           'key' => 'featured_article',
			'value' => '0',
			'compare' => '=='
       )
   )  );
			$loop = new WP_Query( $args );
			?>
		<?php while ( $loop->have_posts() ) : $loop->the_post();?>
               <article>
		<div class="row">
			<div class="col-sm-9 col-xs-12">
		
				<h3 style="margin-bottom: 0px; margin-top: 0px;"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
				</div>
			<div class="col-sm-3 col-xs-12">
				<?php

  $pdf = get_field('pdf_version');
  
  if( !empty($pdf) ): ?>
  <?php $file = get_field('pdf_version');
 
$url = wp_get_attachment_url( $file );
$title = get_the_title( $file );

// part where to get the filesize
$filesize = filesize( get_attached_file( $file ) );
$filesize = size_format($filesize, 0);
?>
<div class="pull-right">
    <a href="<?php echo $url;?>"><button type="button" class="btn btn-default"  data-toggle="tooltip" data-placement="bottom" title=".pdf - <?php echo $filesize; ?>" >
  <span class="glyphicon glyphicon-file" style="color: #00ABDF;"></span> PDF 
</button></a></div>

   <?php endif ?>
				</div>
			</div>
		
                <p><small><?php
$posttags = get_the_tags();
if ($posttags) {
  foreach($posttags as $tag) {
    echo $tag->name . ' - '; 
  }
}?><time datetime="<?php the_time( 'd.m.Y' ); ?>"><?php the_time( 'd.m.Y' ); ?></time><br/> <?php

$categories = get_the_category();
$separator = ' | ';
$output = '';
$i=0;
if($categories){
	foreach($categories as $category) {
		if ($i==2) break;
		$output .= ''.$category->cat_name.''.$separator;
		$i++;
	}
echo trim($output, $separator);
}


?> || <?php if ( function_exists( 'coauthors_posts_links' ) ) {
    coauthors();
} else {
    the_author_posts_link();
} ?></small></p>
		
                 <p class="byline"><?php the_advanced_excerpt('length=12&length_type=words&no_custom=1&finish=sentence&exclude_tags=img,p,strong,h1,h2,h3,h4,h5&read_more=%26#187;'); ?></p>
                
		<hr>
               </article>
	      

	       <?php
	       
	       
	       
	       endwhile;
	       
	       	      
	   	//Navigation Code
	 if ( $loop->max_num_pages > 1 ) { ?>
        <nav class="pagination">
            <h5 class="assistive-text"><?php _e( 'Post navigation', 'textdomain' ); ?></h5>
                <?php

// next_posts_link() usage with max_num_pages
//next_posts_link( 'Older Entries', $loop->max_num_pages );
//previous_posts_link( 'Newer Entries' );
?>

<?php
$big = 999999999; // need an unlikely integer

echo paginate_links( array(
	'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
	'format' => '?paged=%#%',
	'current' => max( 1, get_query_var('paged') ),
	'total' => $loop->max_num_pages
) );
?>

        </nav><!-- #nav -->
        
    	<?php 
	}
	
 else { ?>

	<article id="post-0" class="post no-results not-found">
		<header class="entry-header">
			
		</header><!-- .entry-header -->
	</article><!-- #post-0 -->   
	      <?php } 
	       
	       	wp_reset_postdata();

	       ?>
             
             </div>
		<div class="col-sm-3 col-xs-12" style="padding-left: 35px;">
			<h3><?php _e('Jump to','gunandpartners'); ?></h3>
			<hr>
			<?php
			$tags = get_tags();
			$html = '<ul class="list-unstyled">';
			foreach ( $tags as $tag ) {
				$tag_link = get_tag_link( $tag->term_id );
						
				$html .= "<li style=\"text-transform: uppercase; padding-bottom: 7px;\"><a href='{$tag_link}' title='{$tag->name} Tag' class='{$tag->slug}'>";
				$html .= "{$tag->name}</a></li>";
			}
			$html .= '</ul>';
			echo $html; ?>	    
		</div>
	</div>
	
				<?php  endwhile;
			?>

		</div><!-- #content -->
	</div><!-- #primary -->
	
</div><!-- #main-content -->
<!--</div></div>-->
</div></div>
<?php

get_footer(); ?>
