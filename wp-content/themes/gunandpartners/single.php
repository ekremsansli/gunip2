<?php
/**
 * The template for displaying all posts
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 * Exciting Single Post
 * @package WordPress
 * @subpackage Gun + Partners
 * @since Gun + Partners 0.1
 */

get_header(); ?>
<?php get_sidebar(); ?>

<div id="main-content" class="main-content">

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
<div class="row post">
<?php 
if ( has_post_thumbnail() ) : ?>

<?php  // check if the post has a Post Thumbnail assigned to it.
        $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'header' );
  //the_post_thumbnail('header', array('class' => 'img-responsive scale'));
 
?>	
	<div class="col-12 col-sm-12 col-lg-12 col-xs-12 headerImage" style="
	background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(0,0,0,0) 59%, rgba(0,0,0,0.65) 100%), url(<?php echo $thumb['0'];?>) no-repeat center center; 
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(59%,rgba(0,0,0,0)), color-stop(100%,rgba(0,0,0,0.65))), url(<?php echo $thumb['0'];?>) no-repeat center center; 
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 59%,rgba(0,0,0,0.65) 100%), url(<?php echo $thumb['0'];?>) no-repeat center center; 
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 59%,rgba(0,0,0,0.65) 100%), url(<?php echo $thumb['0'];?>) no-repeat center center;  
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 59%,rgba(0,0,0,0.65) 100%), url(<?php echo $thumb['0'];?>) no-repeat center center; 
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 59%,rgba(0,0,0,0.65) 100%), url(<?php echo $thumb['0'];?>) no-repeat center center; 
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#a6000000',GradientType=0 ), url(<?php echo $thumb['0'];?>') no-repeat center center; 
  -webkit-background-size: 30px 44px, cover, cover;
  -moz-background-size: 30px 44px, cover, cover;
  -o-background-size: 30px 44px, cover, cover;
  background-size: 30px 44px, cover, cover;
  ">

	</div>
	    <?php endif ?>
	<div class="col-12 col-sm-12 col-lg-12 col-xs-12">
<div class="row">
          <div class="col-sm-12 col-md-8 ">
		<?php
				// Start the Loop.
				while ( have_posts() ) : the_post(); ?>
            <h1 class="pageTitle" lang="<?=ICL_LANGUAGE_CODE?>"><?php the_title(); ?></h1>
 <!--DATE--><div class="byline">
 <?php the_date();?><br/>
 <!--TAG-->
			<?php
				$posttags = get_the_tags();
				if ($posttags) {
				  foreach($posttags as $tag) {
				    echo $tag->name . ' '; 
				  }
				  echo '&#8212;';
				}
			?> 
			
			<?php if ( function_exists( 'coauthors_posts_links' ) ) {
				coauthors();
				} else {
				the_author_posts_link();
				} ?>
				<span class="visible-xs visible-sm">
				<?php
				$categories = get_the_category();
				$separator = ' &#8212; ';
				$output = '';
				if($categories){
					foreach($categories as $category) {
						$output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator;
					}
				echo trim($output, $separator);
				}
	?> 
				</span>

		</div>
	</div>
	<div class="col-sm-2 col-md-4 col-xs-12">
		<ul class="list-unstyled corePracticeAreas hidden-xs hidden-sm">
				<?php
				$categories = get_the_category();
				$separator = ' ';
				$output = '';
				if($categories){
					foreach($categories as $category) {
						$output .= '<li><a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">'.$category->cat_name.'</a></li>'.$separator;
					}
				echo trim($output, $separator);
				}
	?> 
	   </ul>

	   
	   </div></div></div>
</div>
<hr>
<div class="row">
	<div class="col-sm-9 col-md-8 col-xs-12">


					<?php the_content(); ?>

	  </div>
	<div class="col-sm-3 col-md-4 col-xs-12">


 <?php //do_action('icl_language_selector'); ?>
   
   
    <?php //icl_post_languages_article(); ?>
        
<?php

  $pdf = get_field('pdf_version');
  
  if( !empty($pdf) ): ?>
  <?php $pdf_file = get_field('pdf_version');
 
$url = wp_get_attachment_url( $pdf_file );
$title = get_the_title( $pdf_file );

// part where to get the filesize
$filesize = filesize( get_attached_file( $pdf_file ) );
$filesize = size_format($filesize, 0);
//$filesize = number_format($filesize / 1048576, 4) . ' MB';
?>
    <a href="<?php echo $url;?>"><button type="button" class="btn btn-default"  data-toggle="tooltip" data-placement="bottom" title=".pdf - <?php echo $filesize; ?>" >
  <span class="glyphicon glyphicon-file" style="color: #00ABDF;"></span> PDF 
</button></a>

   <?php endif ?>

   <?php
  
  $word = get_field('word_version');
  
  if( !empty($word) ): ?>
  <?php $word_file = get_field('word_version');
 
$url = wp_get_attachment_url( $word_file );
$title = get_the_title( $word_file );

// part where to get the filesize
$filesize = filesize( get_attached_file( $word_file ) );
$filesize = size_format($filesize, 0);
//$filesize = number_format($filesize / 1048576, 4) . ' MB';
?>
    <a href="<?php echo $url;?>"><button type="button" class="btn btn-default"  data-toggle="tooltip" data-placement="bottom" title=".doc - <?php echo $filesize; ?>" >
  <span style="color: #00ABDF; font-weight:800;">W</span> Microsoft Word 
</button></a>

   <?php endif ?>
   
	  </div>
</div>


<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->
	
</div><!-- #main-content -->

<?php

get_footer(); ?>
