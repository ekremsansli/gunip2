<?php
/**
/*
Template Name: Team2
 * @package WordPress
 * @subpackage Gun + Partners
 * @since Gun + Partners 0.1
 */

get_header(); ?>
<?php get_sidebar(); ?>

<div id="main-content" class="main-content">

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post(); ?>

				<div class="row">
          <div class="col-6 col-sm-6 col-lg-6 col-xs-12">
            <h1 class="pageTitle"><?php the_title(); ?></h1>
            </div>
             

<div class="col-12 col-sm-12 col-lg-12 col-xs-12">
<?php 
 
$image = get_field('header_image');
 
if( !empty($image) ): ?>
 
	<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="img-responsive"/>
 
<?php endif; ?>
 
</div>
</div>
				
				  <div class="row">
  
		<div class="col-9 col-sm-12 col-md-9 col-lg-9 col-xs-12">
			<?php the_content(); ?>
		</div><!--/span-->
		<div class="col-sm-3 col-xs-12">
			<h3>Jump to:</h3>
			<hr>
			<ul class="list-unstyled">
				<li style="text-transform: uppercase; padding-bottom: 7px;">
					<a href='#partners' title='Partners'>Partners</a>
				</li>
				<li style="text-transform: uppercase; padding-bottom: 7px;">
					<a href='#counsel' title='Counsel'>Counsel</a>
				</li>
				<li style="text-transform: uppercase; padding-bottom: 7px;">
					<a href='#senior' title='Senior Associates'>Senior Associates</a>
				</li>
				<li style="text-transform: uppercase; padding-bottom: 7px;">
					<a href='#associates' title='Associates'>Associates</a>
				</li>
				<li style="text-transform: uppercase; padding-bottom: 7px;">
					<a href='#trainees' title='Trainees'>Trainees</a>
				</li>
			</ul>
		</div>
	</div><!--/span-->
	<div class="row">
		<div class="col-xs-12">
			<hr>	
		</div>
	    	<?php /* The loop */ ?>
		<?php
			$args = array( 'post_type' => 'cv', 'posts_per_page' => -1, 'meta_key' => 'role', 'meta_value' => 'partner');
			$loop = new WP_Query( $args );
			$iter = 1; //iterator to insert clearfixes
		?>
			<section id="partners">
			 <div class="col-xs-12"><h2 style="color: #00ABDF;">Partners</h2></div>
				<?php while ( $loop->have_posts() ) : $loop->the_post();?>
					<div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xs-12 homeBox">
						<a href='<?php the_permalink() ?>'><span></span></a>
						<div class="row">
							<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xs-6">
								<figure>	
									<?php 	$image = get_field('image');
										if( !empty($image) ): ?>
										<img src="<?php echo $image['url']; ?>" alt="<?php the_field('name'); ?>" class="img-responsive"/>
										<?php else : ?>
										<img src="<?php echo get_template_directory_uri(); ?>/img/default_pic.jpg" alt="<?php the_field('name'); ?>" class="img-responsive"/>
									<?php endif; ?>
								</figure>
							</div>
							<div class="col-sm-12 col-xs-6">										 
								<h2><?php the_title(); ?></h2>
								<h3>
								<?php
									$field = get_field_object('role');
									$value = get_field('role');
									$label = $field['choices'][ $value ];
									echo $label;
								?>
								</h3><br/>
								<address>
									<p class="hidden-xs">
										<?php the_field('telephone_number'); ?><br/>
										<a href=""><?php the_field('email_address'); ?></a>
									</p>
								</address>
							</div>
						</div>
					</div>
			 
			<?php if( $iter % 3 == 0 ):?>
			<div class="clearfix visible-xs-block"></div>
			<?php endif;
			$iter++;
			?>
			 
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
			</section>
			<div class="col-xs-12">
				<hr>	
			</div>
			<?php
			$args = array( 	'post_type' => 'cv', 'posts_per_page' => -1,
					'meta_query' => array(
						'relation' => 'OR',
						array(
							'key' => 'role',
							'value' => 'counsel',
							'compare' => 'LIKE'
						),
						array(
							'key' => 'role',
							'value' => 'ofcounsel',
							'compare' => 'LIKE'
						)
					)
					);
			
			$loop = new WP_Query( $args );
			$iter = 1; //iterator to insert clearfixes
			?>
			<section id="counsel">
			 <div class="col-xs-12"><h2 style="color: #00ABDF;">Counsel</h2></div>
				<?php while ( $loop->have_posts() ) : $loop->the_post();?>
					<div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xs-12 homeBox">
						<a href='<?php the_permalink() ?>'><span></span></a>
						<div class="row">
							<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xs-6">
								<figure>	
									<?php 	$image = get_field('image');
										if( !empty($image) ): ?>
										<img src="<?php echo $image['url']; ?>" alt="<?php the_field('name'); ?>" class="img-responsive"/>
										<?php else : ?>
										<img src="<?php echo get_template_directory_uri(); ?>/img/default_pic.jpg" alt="<?php the_field('name'); ?>" class="img-responsive"/>
									<?php endif; ?>
								</figure>
							</div>
							<div class="col-sm-12 col-xs-6">										 
								<h2><?php the_title(); ?></h2>
								<h3>
								<?php
									$field = get_field_object('role');
									$value = get_field('role');
									$label = $field['choices'][ $value ];
									echo $label;
								?>
								</h3><br/>
								<address>
									<p class="hidden-xs">
										<?php the_field('telephone_number'); ?><br/>
										<a href=""><?php the_field('email_address'); ?></a>
									</p>
								</address>
							</div>
						</div>
					</div>
			 
			<?php if( $iter % 3 == 0 ):?>
			<div class="clearfix visible-xs-block"></div>
			<?php endif;
			$iter++;
			?>
			 
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
			</section>
			<div class="col-xs-12">
				<hr>	
			</div>
			<?php /* The loop */ ?>
		<?php
			$args = array( 'post_type' => 'cv', 'posts_per_page' => -1, 'meta_key' => 'role', 'meta_value' => 'senior');
			$loop = new WP_Query( $args );
			$iter = 1; //iterator to insert clearfixes
		?>
			<section id="senior">
			 <div class="col-xs-12"><h2 style="color: #00ABDF;">Senior Associates</h2></div>
				<?php while ( $loop->have_posts() ) : $loop->the_post();?>
					<div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xs-12 homeBox">
						<a href='<?php the_permalink() ?>'><span></span></a>
						<div class="row">
							<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xs-6">
								<figure>	
									<?php 	$image = get_field('image');
										if( !empty($image) ): ?>
										<img src="<?php echo $image['url']; ?>" alt="<?php the_field('name'); ?>" class="img-responsive"/>
										<?php else : ?>
										<img src="<?php echo get_template_directory_uri(); ?>/img/default_pic.jpg" alt="<?php the_field('name'); ?>" class="img-responsive"/>
									<?php endif; ?>
								</figure>
							</div>
							<div class="col-sm-12 col-xs-6">										 
								<h2><?php the_title(); ?></h2>
								<h3>
								<?php
									$field = get_field_object('role');
									$value = get_field('role');
									$label = $field['choices'][ $value ];
									echo $label;
								?>
								</h3><br/>
								<address>
									<p class="hidden-xs">
										<?php the_field('telephone_number'); ?><br/>
										<a href=""><?php the_field('email_address'); ?></a>
									</p>
								</address>
							</div>
						</div>
					</div>
			 
			<?php if( $iter % 3 == 0 ):?>
			<div class="clearfix visible-xs-block"></div>
			<?php endif;
			$iter++;
			?>
			 
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
			</section>
				<div class="col-xs-12">
				<hr>	
			</div>
			<?php /* The loop */ ?>
		<?php
			$args = array( 'post_type' => 'cv', 'posts_per_page' => -1, 'meta_key' => 'role', 'meta_value' => 'associate');
			$loop = new WP_Query( $args );
			$iter = 1; //iterator to insert clearfixes
		?>
			<section id="associates">
			 <div class="col-xs-12"><h2 style="color: #00ABDF;">Associates</h2></div>
				<?php while ( $loop->have_posts() ) : $loop->the_post();?>
					<div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xs-12 homeBox">
						<a href='<?php the_permalink() ?>'><span></span></a>
						<div class="row">
							<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xs-6">
								<figure>	
									<?php 	$image = get_field('image');
										if( !empty($image) ): ?>
										<img src="<?php echo $image['url']; ?>" alt="<?php the_field('name'); ?>" class="img-responsive"/>
										<?php else : ?>
										<img src="<?php echo get_template_directory_uri(); ?>/img/default_pic.jpg" alt="<?php the_field('name'); ?>" class="img-responsive"/>
									<?php endif; ?>
								</figure>
							</div>
							<div class="col-sm-12 col-xs-6">										 
								<h2><?php the_title(); ?></h2>
								<h3>
								<?php
									$field = get_field_object('role');
									$value = get_field('role');
									$label = $field['choices'][ $value ];
									echo $label;
								?>
								</h3><br/>
								<address>
									<p class="hidden-xs">
										<?php the_field('telephone_number'); ?><br/>
										<a href=""><?php the_field('email_address'); ?></a>
									</p>
								</address>
							</div>
						</div>
					</div>
			 
			<?php if( $iter % 3 == 0 ):?>
			<div class="clearfix visible-xs-block"></div>
			<?php endif;
			$iter++;
			?>
			 
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
			</section>
			<div class="col-xs-12">
				<hr>	
			</div>
			<?php /* The loop */ ?>
		<?php
			$args = array( 'post_type' => 'cv', 'posts_per_page' => -1, 'meta_key' => 'role', 'meta_value' => 'trainee');
			$loop = new WP_Query( $args );
			$iter = 1; //iterator to insert clearfixes
		?>
			<section id="trainees">
			 <div class="col-xs-12"><h2 style="color: #00ABDF;">Trainees</h2></div>
				<?php while ( $loop->have_posts() ) : $loop->the_post();?>
					<div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xs-12 homeBox">
						<a href='<?php the_permalink() ?>'><span></span></a>
						<div class="row">
							<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xs-6">
								<figure>	
									<?php 	$image = get_field('image');
										if( !empty($image) ): ?>
										<img src="<?php echo $image['url']; ?>" alt="<?php the_field('name'); ?>" class="img-responsive"/>
										<?php else : ?>
										<img src="<?php echo get_template_directory_uri(); ?>/img/default_pic.jpg" alt="<?php the_field('name'); ?>" class="img-responsive"/>
									<?php endif; ?>
								</figure>
							</div>
							<div class="col-sm-12 col-xs-6">										 
								<h2><?php the_title(); ?></h2>
								<h3>
								<?php
									$field = get_field_object('role');
									$value = get_field('role');
									$label = $field['choices'][ $value ];
									echo $label;
								?>
								</h3><br/>
								<address>
									<p class="hidden-xs">
										<?php the_field('telephone_number'); ?><br/>
										<a href=""><?php the_field('email_address'); ?></a>
									</p>
								</address>
							</div>
						</div>
					</div>
			 
			<?php if( $iter % 3 == 0 ):?>
			<div class="clearfix visible-xs-block"></div>
			<?php endif;
			$iter++;
			?>
			 
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
			</section>
			
					
				<?php endwhile;
						?>

		</div><!-- #content -->
	</div><!-- #primary -->
	
</div><!-- #main-content -->
<!--</div></div>-->

<?php

get_footer(); ?>
