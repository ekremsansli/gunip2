<?php
/**
/*
Template Name: Home
 * @package WordPress
 * @subpackage Gun + Partners
 * @since Gun + Partners 0.1
 */

get_header();?>
<?php get_sidebar(); ?>

<div id="main-content" class="main-content">

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post(); ?>

				<div class="row">
          <!--<div class="col-6 col-sm-6 col-lg-6 col-xs-12">
            <h1 class="pageTitle"><?php the_title(); ?></h1>
            </div>-->
             

<div class="col-12 col-sm-12 col-lg-12 col-xs-12">
<?php 
 
$image = get_field('header_image');
 
if( !empty($image) ): ?>	
 
	<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="img-responsive"/>
 
<?php endif; ?>
  <!--<button type="button" class="btn btn-default" style="float: right; border: none;">
  <span class="glyphicon glyphicon-picture" style="color: #00ABDF;"></span>  Our Office
</button>-->
</div>
</div>
				<div class="row">
          <div class="col-lg-12 hidden-xs hidden-sm">
            <div class="about">
              <div class="col-7 col-sm-7 col-lg-7 col-xs-7">
<!--		<h2>Links</h2>
		<h2><?php// _e('Links','theme-text-domain'); ?></h2>-->
                <div class="aboutWhite"><?php _e("<p>Gün + Partners Patent - Trademark, Türkiye'de doğrudan, dünya çapında ise işbirliği yaptığı uluslararası alanda saygın bürolar aracılığıyla hizmet veren, Türkiye'nin önde gelen tescil bürolarından birisidir.</p>",'gunandpartners'); ?>
               		<a href="<?php echo get_permalink(icl_object_id(1008, 'page', true)); ?>"><?php echo get_the_title(icl_object_id(1008, 'page', true)); ?></a>
				</div>
              </div>
            </div>
            
          </div>
</div>
				  <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12 homeBox visible-xs visible-sm">
		<?php icl_link_to_element(2,'page','<img src="'.get_template_directory_uri().'/img/buyuk.png" class="img-responsive">'); ?>	 
              
            <?php _e("<p>Gün + Partners Patent - Trademark, Türkiye'de doğrudan, dünya çapında ise işbirliği yaptığı uluslararası alanda saygın bürolar aracılığıyla hizmet veren, Türkiye'nin önde gelen tescil bürolarından birisidir.</p>",'gunandpartners'); ?>
               		<a href="<?php echo get_permalink(icl_object_id(1008, 'page', true)); ?>"><?php echo get_the_title(icl_object_id(1008, 'page', true)); ?></a>
              <p><?php
		?></p>
			 
              <hr>
		
            </div><!--/span-->
            <div class="col-md-4 col-sm-6 col-xs-12 homeBox">
              
	       <?php icl_link_to_element(1039,'page','<img src="'.get_template_directory_uri().'/img/expertise.png" class="img-responsive">'); ?>	 
             <h2 lang="<?=ICL_LANGUAGE_CODE?>"><?php _e('Stratejik<br/> Hizmetlerimiz','gunandpartners'); ?></h2>
              <p><?php _e('Etkin bir fikri mülkiyet stratejisi işletmelerin rekabetçiliğinin olmazsa olmaz şartıdır. Uygun bir fikri mülkiyet stratejisi olmayan işletmeler yarattıkları değerleri kolaylıkla rakiplerine kaptırabilirler.')?></p>
               <p>
		<?php
		icl_link_to_element(1039,'page',__('Stratejik Hizmetlerimiz     ','gunandpartners'));
		?>
		</p>
              <hr>
            </div><!--/span-->
            <div class="col-md-4 col-sm-6 col-xs-12 homeBox">
                            
 <?php icl_link_to_element(1178,'page','<img src="'.get_template_directory_uri().'/img/international.png" class="img-responsive">'); ?>	 
	      <h2 lang="<?=ICL_LANGUAGE_CODE?>"><?php _e('Uluslararası','gunandpartners'); ?></h2>
	      <p><?php _e('Gün+Partners Patent ve Marka Tescil Bürosu fikri mülkiyet haklarının tescili ve korunmasında fikri mülkiyet alanında uzmanlaşmış bürolardan oluşan işbirliği ağıyla müvekkillerine yurtdışında da hizmet vermektedir.','gunandpartners'); ?></p>
              <p>
		<?php
		icl_link_to_element(1178,'page',__('Uluslararası','gunandpartners'));
		?>
		</p>
              
              <hr>
            </div><!--/span-->
            <div class="col-md-4 col-sm-6 col-xs-12 homeBox">
<?php icl_link_to_element(960,'page','<img src="'.get_template_directory_uri().'/img/faqs.jpg" class="img-responsive">'); ?>	 
              <h2><?php _e('Sıkça Sorulan Sorular','gunandpartners'); ?></h2>
              <p><?php _e('Bu bölümde bizlere sıkça sorulan soruların genel olarak kabul gören cevaplarını bulabilirsiniz. Bu cevaplar, sadece genel bilgilendirme amacı taşır ve sorunlarınız hakkında fikir verebilmeyi amaçlamaktadır. Ancak, bu bölümde yer alan cevaplar hukuki mütalaa veya tavsiye değildir.','gunandpartners'); ?></p>
              <p>
		<?php
			icl_link_to_element(960,'page',__('Sıkça Sorulan Sorular','gunandpartners'));
		?>
		</p>
              <hr>
             <!-- <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>-->
            </div><!--/span-->

          </div><!--/row-->
				
				
					
				<?php endwhile;
			?>

		</div><!-- #content -->
	</div><!-- #primary -->
	
</div><!-- #main-content -->


<?php

get_footer(); ?>
