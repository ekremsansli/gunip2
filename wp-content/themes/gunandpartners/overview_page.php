<?php
/**
/*
Template Name: Overview
 * @package WordPress
 * @subpackage Gun + Partners
 * @since Gun + Partners 0.1
 */

get_header(); ?>
<?php get_sidebar(); ?>
<?php 
  $sections = array(
      1058 => 'arastirma',
      1054 => 'arastirma',
      1035 =>'stratejik',
      1037 =>'stratejik',
      1113 =>'taninmis-markalar',
      1116 =>'taninmis-markalar',
      1119 => 'tescil',
      1121 => 'tescil',
      1170 => 'itiraz',
      1172 => 'itiraz',
      1178 => 'uluslararasi',
      1180 => 'uluslararasi'
    );

?>

<div id="main-content" class="main-content">

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post(); ?>

				<div class="row">
          <div class="col-6 col-sm-6 col-lg-6 col-xs-12">
            <h1 class="pageTitle" lang="<?=ICL_LANGUAGE_CODE?>"><?php _e('Hizmetlerimiz                 ','gunandpartners'); ?></h1>
            </div>
             


<?php  // check if the post has a Post Thumbnail assigned to it.
        $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'header' );
  //the_post_thumbnail('header', array('class' => 'img-responsive scale'));
 
?>	
	<div class="col-12 col-sm-12 col-lg-12 col-xs-12 headerImage" style="
	background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(0,0,0,0) 59%, rgba(0,0,0,0.65) 100%), url(http://gun.av.tr/wp-content/uploads/2014/05/pg80396-cropped-1698x449.jpg) no-repeat center center; 
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(59%,rgba(0,0,0,0)), color-stop(100%,rgba(0,0,0,0.65))), url(http://gun.av.tr/wp-content/uploads/2014/05/pg80396-cropped-1698x449.jpg) no-repeat center center; 
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 59%,rgba(0,0,0,0.65) 100%), url(http://gun.av.tr/wp-content/uploads/2014/05/pg80396-cropped-1698x449.jpg) no-repeat center center; 
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 59%,rgba(0,0,0,0.65) 100%), url(http://gun.av.tr/wp-content/uploads/2014/05/pg80396-cropped-1698x449.jpg) no-repeat center center;  
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 59%,rgba(0,0,0,0.65) 100%), url(http://gun.av.tr/wp-content/uploads/2014/05/pg80396-cropped-1698x449.jpg) no-repeat center center; 
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 59%,rgba(0,0,0,0.65) 100%), url(http://gun.av.tr/wp-content/uploads/2014/05/pg80396-cropped-1698x449.jpg) no-repeat center center; 
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#a6000000',GradientType=0 ), url('http://gun.av.tr/wp-content/uploads/2014/05/pg80396-cropped-1698x449.jpg') no-repeat center center; 
  -webkit-background-size: 30px 44px, cover, cover;
  -moz-background-size: 30px 44px, cover, cover;
  -o-background-size: 30px 44px, cover, cover;
  background-size: 30px 44px, cover, cover;
  ">

	</div>

</div>
				
				  <div class="row">
              
             <div class="col-8 col-sm-12 col-md-8 col-lg-8 col-xs-12 scroll-links" style="margin-top:34px;">
              <?php the_content_by_id(icl_object_id(4772, 'page', true));?>
              <?php the_content_by_id(icl_object_id(4791, 'page', true));?>

            
            </div><!--/span-->
           <div class="col-4 col-sm-12 col-md-4 col-lg-4 col-xs-12 right_menu scroll-links" style="padding-top: 25px;">
               <!-- <div data-spy="affix">--><!--data-offset-top="60" data-offset-bottom="0"--> 
                <div data-spy="affix" data-offset-top="422" class="affix-top">
                <h4><?php _e('Hizmetlerimiz','gunandpartners') ?></h4>
		            <ul class="nav sidebar">
                  <li><a href="#stratejik" class="open-menu"><?php echo get_the_title(icl_object_id(1035, 'page', true)); ?></a></li>
                  <ul class="nav sidebar stratejikul submenu" style="display:none;">
                    <li><a href="#fms"><?php echo get_the_title(icl_object_id(1039, 'page', true)); ?></a></li>
                    <li><a href="#fmpy"><?php echo get_the_title(icl_object_id(1043, 'page', true)); ?></a></li>
                    <li><a href="#fmhei"><?php echo get_the_title(icl_object_id(1049, 'page', true)); ?></a></li>
                  </ul>
                  
                  <li><a href="#arastirma" class="open-menu"><?php echo get_the_title(icl_object_id(1054, 'page', true)); ?></a></li>
                  <ul class="nav sidebar arastirmaul submenu" style="display:none;">
                    <li><a href="#bvta"><?php echo get_the_title(icl_object_id(1065, 'page', true)); ?></a></li>
                    <li><a href="#uba"><?php echo get_the_title(icl_object_id(1069, 'page', true)); ?></a></li>
                    <li><a href="#ba"><?php echo get_the_title(icl_object_id(1073, 'page', true)); ?></a></li>
                    <li><a href="#dhba"><?php echo get_the_title(icl_object_id(1079, 'page', true)); ?></a></li>
                    <li><a href="#piva"><?php echo get_the_title(icl_object_id(1082, 'page', true)); ?></a></li>
                    <li><a href="#pbvta"><?php echo get_the_title(icl_object_id(1088, 'page', true)); ?></a></li>
                    <li><a href="#ya"><?php echo get_the_title(icl_object_id(1092, 'page', true)); ?></a></li>

                  </ul>
                  
                  <li><a href="#taninmis-markalar" class="open-menu"><?php echo get_the_title(icl_object_id(1113, 'page', true)); ?></a></li>
                  <li><a href="#tescil" class="open-menu"><?php echo get_the_title(icl_object_id(1119, 'page', true)); ?></a></li>
                  <ul class="nav sidebar tescilul submenu" style="display:none;">
                    <li><a href="#mt"><?php echo get_the_title(icl_object_id(1123, 'page', true)); ?></a></li>
                    <li><a href="#pt"><?php echo get_the_title(icl_object_id(1129, 'page', true)); ?></a></li>
                    <li><a href="#fmt"><?php echo get_the_title(icl_object_id(1136, 'page', true)); ?></a></li>
                    <li><a href="#ett"><?php echo get_the_title(icl_object_id(1138, 'page', true)); ?></a></li>
                    <li><a href="#aat"><?php echo get_the_title(icl_object_id(1142, 'page', true)); ?></a></li>
                    <li><a href="#yvu"><?php echo get_the_title(icl_object_id(1148, 'page', true)); ?></a></li>
                    <li><a href="#idvg"><?php echo get_the_title(icl_object_id(1152, 'page', true)); ?></a></li>
                    <li><a href="#dth"><?php echo get_the_title(icl_object_id(1157, 'page', true)); ?></a></li>

                  </ul>
                  <li><a href="#izleme" class="open-menu"><?php echo get_the_title(icl_object_id(1170, 'page', true)); ?></a></li>
                  <li><a href="#itiraz" class="open-menu"><?php echo get_the_title(icl_object_id(1174, 'page', true)); ?></a></li>
                  <li><a href="#uluslararasi" class="open-menu"><?php echo get_the_title(icl_object_id(1178, 'page', true)); ?></a></li>
                  <ul class="nav sidebar uluslararasiul submenu" style="display:none;">
                    <li><a href="#umt"><?php echo get_the_title(icl_object_id(1182, 'page', true)); ?></a></li>
                    <li><a href="#upfmt"><?php echo get_the_title(icl_object_id(1186, 'page', true)); ?></a></li>
                    <li><a href="#uett"><?php echo get_the_title(icl_object_id(1191, 'page', true)); ?></a></li>
                    <li><a href="#uyvu"><?php echo get_the_title(icl_object_id(1196, 'page', true)); ?></a></li>
                    <li><a href="#uidvg"><?php echo get_the_title(icl_object_id(1200, 'page', true)); ?></a></li>
                    <li><a href="#uih"><?php echo get_the_title(icl_object_id(1211, 'page', true)); ?></a></li>
                    <li><a href="#ui"><?php echo get_the_title(icl_object_id(1215, 'page', true)); ?></a></li>
                  </ul>
                  </ul>
                  
                  </li>
                 
                </ul>
              </div>
           <!-- </div> -->
          </div><!--/row-->

        </div><!--/span-->
				
					<!--the_content();

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) {
						comments_template();
					}-->
					
				<?php endwhile;
			?>

		</div><!-- #content -->
	</div><!-- #primary -->
</div><!-- #main-content -->

<script type="text/javascript">
jQuery(window).load(function($){
    var target = jQuery("#<?=$sections[get_the_ID()]?>");
        jQuery('a[href=#<?=$sections[get_the_ID()]?>]').parent().addClass('active');
        jQuery(".<?=$sections[get_the_ID()]?>ul").show();
        jQuery('html,body').animate({
          scrollTop: target.offset().top-100
        }, 1000);
        jQuery('.right_menu a').click(function(){
          var ab = jQuery(jQuery(this).attr('href'));
          jQuery('html,body').animate({
          scrollTop: ab.offset().top-100
        }, 1000);
          jQuery('.right_menu a').parent().removeClass('active');
          jQuery(this).parent().addClass('active');
        });
        jQuery('.right_menu .open-menu').click(function(){
          var ff = jQuery(this).attr('href').replace('#','');
          jQuery('.submenu').hide();
          jQuery("."+ff+"ul").show();
        }); 

})

</script>
<?php

get_footer(); ?>
