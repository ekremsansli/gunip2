<?php
/**
/*
Template Name: Discover
 * @package WordPress
 * @subpackage Gun + Partners
 * @since Gun + Partners 0.1
 */

get_header(); ?>
<?php get_sidebar(); ?>

<div id="main-content" class="main-content">

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

                    <?php
                              $args = my_search_args();
                              $my_search = new WP_Advanced_Search($args);
                              $temp_query = $wp_query;
                              $wp_query = $my_search->query();
                    ?>
				<div class="row">
          <div class="col-10 col-sm-12 col-lg-10 col-xs-12"><br/><br/>
                    <?php if(get_search_query()) : ?>
                   <!-- <p>You searched for " <?php //echo esc_html( get_search_query( false ) ); ?> ". Here are the results:</p>-->
		    <p>
		    <?php
		    printf(
			__( 'You searched for "%s". Here are the results:', 'gunandpartners' ), esc_html(get_search_query( false ))); ?>
		    </p>
                      <?php else : ?>
                      <p><?php _e('Latest Insights','gunandpartners'); ?></p>
                    <?php endif; ?>
                    <hr>
           
</div>
             

</div>
<?php
				// Start the Loop.
                                if ( have_posts() ): 
				while ( have_posts() ) : the_post(); ?>


<div class="row">
                     <div class="col-10 col-sm-12 col-lg-10 col-xs-12">				
<article>
		<div class="row">
			<div class="col-xs-3 hidden-xs">
				<?php if ( has_post_thumbnail() ) : ?>
				<a href="<?php the_permalink() ?>"> <?php the_post_thumbnail('medium', array('class' => 'img-thumbnail')); ?></a>
				<?php else : ?>
				<a href="<?php the_permalink() ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/searchDefault.png" class="img-thumbnail" style="opacity: 0.75"></a>
				<?php endif ?>
			</div>
			<div class="col-sm-9 col-xs-12">
				<div class="row">
			<div class="col-sm-8 col-xs-12">
		
				<h3 style="margin-bottom: 0px; margin-top: 0px;"><a href="<?php the_permalink() ?>"><?php relevanssi_the_title() ?></a></h3>
				</div>
			<div class="col-sm-4 col-xs-12">
				<?php

  $pdf_file = get_field('pdf_version');
  
  if( !empty($pdf_file) ): ?>
  <?php $pdf_file = get_field('pdf_version');
 
$url = wp_get_attachment_url( $pdf_file );
$title = get_the_title( $pdf_file );

// part where to get the filesize
$filesize = filesize( get_attached_file( $pdf_file ) );
$filesize = size_format($filesize, 0);
?>
    <a href="<?php echo $url;?>"><button type="button" style="float:right; "class="btn btn-default"  data-toggle="tooltip" data-placement="bottom" title=".pdf - <?php echo $filesize; ?>" >
  <span class="glyphicon glyphicon-file" style="color: #00ABDF;"></span> PDF 
</button></a>

   <?php endif ?>
				</div>
				</div>
			
		
			
		
                <p><small><?php
$posttags = get_the_tags();
if ($posttags) {
  foreach($posttags as $tag) {
    echo $tag->name . ' - '; 
  }
}?><time datetime="<?php the_time( 'd.m.Y' ); ?>"><?php the_time( 'd.m.Y' ); ?></time><br/> <?php

$categories = get_the_category();
$separator = ' | ';
$output = '';
$i=0;
if($categories){
	foreach($categories as $category) {
		if ($i==2) break;
		$output .= ''.$category->cat_name.''.$separator;
		$i++;
	}
echo trim($output, $separator);
}


?> || <?php if ( function_exists( 'coauthors_posts_links' ) ) {
    coauthors();
} else {
    the_author_posts_link();
} ?></small></p>
		
                 <p class="byline"><?php
		 the_excerpt();
		 //the_advanced_excerpt('length=12&length_type=words&no_custom=1&finish=sentence&exclude_tags=img,p,strong,h1,h2,h3,h4,h5&read_more=%26#187;');
		 ?></p>

		
                                    </div></div><hr></div>
</div>
               </article>
					
				<?php endwhile;
                                
                             $my_search->pagination(array('prev_text' => '&#171;','next_text' => '&#187;'));
                                
                                
			else :

				 _e('Sorry, no posts matched your criteria.', 'gunandpartners');

			endif;
			
			$wp_query = $temp_query;
			wp_reset_query();
			?>

		</div><!-- #content -->
	</div><!-- #primary -->
	
</div><!-- #main-content -->


<?php

get_footer(); ?>
