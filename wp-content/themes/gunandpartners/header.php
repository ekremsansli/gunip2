<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico">
      <link href="<?php echo get_template_directory_uri(); ?>/img/apple-touch-icon.png" rel="apple-touch-icon" />
<link href="<?php echo get_template_directory_uri(); ?>/img/apple-touch-icon-76x76.png" rel="apple-touch-icon" sizes="76x76" />
<link href="<?php echo get_template_directory_uri(); ?>/img/apple-touch-icon-120x120.png" rel="apple-touch-icon" sizes="120x120" />
<link href="<?php echo get_template_directory_uri(); ?>/img/apple-touch-icon-152x152.png" rel="apple-touch-icon" sizes="152x152" />

    <title><?php
global $page, $paged;
	wp_title( '|', true, 'right' );
	bloginfo( 'name' );
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";?></title>

 <link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" />

 
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="//use.typekit.net/wks4jqc.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
     <script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBmj8am6ulGmMvFIu-JYQja9tlIEqoYqSA">
    </script>
     
    <?php wp_head(); ?>
    <script type="text/javascript">
      jQuery(document).ready(function($){        
        $.each($('.right_menu').find('a'),function(key,value){
          if($(value).attr('href') == window.location.href){
            $(value).parent().addClass('active');
          }
        });
      });
    </script>
  </head>

  <body <?php body_class(); ?> data-spy="scroll" data-target=".practice" style="position: relative">
    <div class="crosses hidden-xs"></div>

     <div class="navWrap">
    <div id="nav-wrapper" data-spy="affix" data-offset-top="44">

    <div class="navbar navbar-top navbar-default" role="navigation">
      <div class="container padded">

        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
          <a class="navbar-brand" href="<?php echo home_url(); ?>">Gun + Partners</a>

        </div>

          <?php
            wp_nav_menu( array(
                'menu'              => 'primary',
                'theme_location'    => 'header-menu',
                'depth'             => 2,
                'container'         => 'div',
                'container_class'   => 'collapse navbar-collapse navbar-ex1-collapse',
                'menu_class'        => 'nav navbar-nav navbar-right',
                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                'walker'            => new wp_bootstrap_navwalker())
            );
        ?>


      </div><!-- /.container -->
    </div><!-- /.navbar -->
    </div> <!--/.navbar wrapper-->
    <div>
      <div class="container" style="border-bottom: 1px solid #eee;">
        <div class="row">
          <div class="col-sm-8 col-xs-12 hidden-xs">  <ol class="breadcrumb">
          <?php if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb('<p id="breadcrumbs">','</p>');
} ?>
</ol></div>

          <div class="col-sm-4 col-xs-12">
            <!--<ul class="list-inline" style="float: right;">-->
              <!--<li><a href="#" class="text-muted">Turkish</a></li>-->

              <?php do_action('icl_language_selector'); ?>
              <!--<li><a href="#">English</a></li>-->
            <!--</ul>-->
          </div>
         </div>
      </div>
    </div>
     </div>
     <?php
    
                        ?>
        <div class="container">