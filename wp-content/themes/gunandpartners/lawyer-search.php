<?php
/**
/*
Template Name: Lawyer Search
 * @package WordPress
 * @subpackage Gun + Partners
 * @since Gun + Partners 0.1
 */

get_header(); ?>
<?php get_sidebar(); ?>

<div id="main-content" class="main-content">

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

                    <?php
                              $args = cv_search_args();
                              $my_search = new WP_Advanced_Search($args);
                              $temp_query = $wp_query;
                              $wp_query = $my_search->query();
                    ?>
				<div class="row">
          <div class="col-9 col-sm-12 col-lg-9 col-xs-12"><br/><br/>
                    <?php if(get_search_query()) : ?>
                    <p>
			
                      <?php
		      
		      $postNum = $wp_query->found_posts;
		   printf(
			__( 'You searched for "%s". ', 'gunandpartners' ), esc_html(get_search_query( false ))
			);
		   printf(
			__( 'There are %s people that match your query:', 'gunandpartners' ), $wp_query->found_posts
			);
		   
		   ?></p>
		      <?php else : ?>
                      <p> <?php _e('All Lawyers', 'gunandpartners'); ?> (<?php echo  $wp_query->found_posts; ?>)</p>
                    <?php endif; ?>
                    <hr>
           
</div>
             

</div>
<?php
				// Start the Loop.
                                if ( have_posts() ): 
				while ( have_posts() ) : the_post(); ?>


<div class="row">
	
	

		<div class="col-lg-9 col-md-12 lawyer">
			<div class="row homeBox">
				<a href='<?php the_permalink() ?>'><span></span></a>
	<div class="col-md-4 col-sm-4 col-xs-6 ">
		
		<figure>
				
			<?php 	$image = get_field('image');
				if( !empty($image) ): ?>
 
			<img src="<?php echo $image['url']; ?>" alt="<?php the_field('name'); ?>" class="img-responsive"/>
			<?php else : ?>
								<img src="<?php echo get_template_directory_uri(); ?>/img/default_pic.jpg" alt="<?php the_field('name'); ?>" class="img-responsive"/>
							
			<?php endif; ?>
			</figure>
			 </div>
			 <div class="col-md-3 col-sm-4  col-xs-6">										 
			<h4><?php the_title(); ?></h4>
			<h5><?php
			$field = get_field_object('role');
			$value = get_field('role');
			$label = $field['choices'][ $value ];
			echo $label; ?></h5>
			<address>
			<p class="hidden-xs">
			  <small><?php the_field('telephone_number'); ?></small><br/>
			  <small><?php the_field('email_address'); ?></small>
			  
			</p>
			</address>
			</div>
			 <div class="col-md-5 col-sm-4 hidden-xs">
				<h5 style="text-transform:uppercase;"><?php //_e('Practice Areas','gunandpartners'); ?></h5>
				<?php	
				//$cats = get_field('core_practice_areas');
				//echo '<ul class="list-unstyled">';
				//foreach ($cats as $key => $val){
				//    echo '<li><small>'.$val.'</small></li>';
				//}
				//echo '</ul>';
				//the_excerpt();
				?>
			 </div>
			</div>
		</div>
			
</div><br/>
					
				<?php endwhile;
                                
                             $my_search->pagination(array('prev_text' => '&#171;','next_text' => '&#187;'));
                                
                                
			else :

				_e('Sorry, no posts matched your criteria.', 'gunandpartners');

			endif;
			
			$wp_query = $temp_query;
			wp_reset_query();
			?>

		</div><!-- #content -->
	</div><!-- #primary -->
	
</div><!-- #main-content -->


<?php

get_footer(); ?>
