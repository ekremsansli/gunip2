<?php
/**
/*
Template Name: Team_XX
 * @package WordPress
 * @subpackage Gun + Partners
 * @since Gun + Partners 0.1
 */

get_header(); ?>
<?php get_sidebar(); ?>

<div id="main-content" class="main-content">

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post(); ?>

				<div class="row">
          <div class="col-6 col-sm-6 col-lg-6 col-xs-12">
            <h1 class="pageTitle"><?php the_title(); ?></h1>
            </div>
             

<div class="col-12 col-sm-12 col-lg-12 col-xs-12">
<?php 
 
$image = get_field('header_image');
 
if( !empty($image) ): ?>
 
	<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="img-responsive"/>
 
<?php endif; ?>
 
</div>
</div>
				
				  <div class="row">
  
             <div class="col-9 col-sm-12 col-md-9 col-lg-9 col-xs-12">
<?php the_content(); ?>

<!--<h2>List of authors:</h2>
<ul>
<?php //wp_list_authors('exclude_admin=1&hide_empty=0'); ?>
</ul>-->
            
            </div><!--/span-->
	      </div><!--/span-->
	      <div class="row">
		<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xs-12">
		<hr>
			
		</div>
	    	<?php /* The loop */ ?>
		<?php
			$args = array( 'post_type' => 'cv', 'posts_per_page' => -1 );
			$loop = new WP_Query( $args );
			$iter = 1; //iterator to insert clearfixes
			?>
		<?php while ( $loop->have_posts() ) : $loop->the_post();?>
			 <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xs-12 homeBox">
				<a href='<?php the_permalink() ?>'><span></span></a>
				<div class="row">
			 <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xs-6"><figure>
				
			<?php 	$image = get_field('image');
				if( !empty($image) ): ?>
 
			<img src="<?php echo $image['url']; ?>" alt="<?php the_field('name'); ?>" class="img-responsive"/>
	 
			<?php endif; ?>
			</figure>
			 </div>
			 <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xs-6">										 
			<h2><?php the_title(); ?></h2>
			<h3><?php
			$field = get_field_object('role');
			$value = get_field('role');
			$label = $field['choices'][ $value ];
			echo $label; ?></h3><br/>
			<address>
			<p class="hidden-xs">
			  <?php the_field('telephone_number'); ?><br/>
			  <a href=""><?php the_field('email_address'); ?></a>
			</p>
			</address>
			</div>
								 
	
								
							
						
					</div></div>
			 
			 <?php 	$image = get_field('image');
			 //Should be using Modulo operator here! 
				if( $iter == 3 || $iter == 6 || $iter == 9 || $iter == 12 ): ?>
 
			<div class="clearfix visible-xs-block"></div>
	 
			<?php
			
			
			endif;
			
			$iter++;?>
			 
					<?php endwhile; ?>


        
				
					<!--the_content();

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) {
						comments_template();
					}-->
					
				<?php endwhile;
			?>

		</div><!-- #content -->
	</div><!-- #primary -->
	
</div><!-- #main-content -->
<!--</div></div>-->

<?php

get_footer(); ?>
