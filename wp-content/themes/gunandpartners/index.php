<?php

define('WPAS_DEBUG', true);
//define( 'WP_DEBUG', true );
get_header(); ?>
<?php get_sidebar(); ?>
<div class="row">
          <div class="col-lg-12 hidden-xs">

            
              <br/><br/>
          </div>
          
</div>
<div class="row">
     <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xs-12">
    <div class="content">
        

<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>



<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="post-header">

<h2><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
<div class="date"><?php the_time( 'd.m.Y' ); ?>
</div>
<div class="">
<?php

$categories = get_the_category();
$separator = ' | ';
$output = '';
if($categories){
	foreach($categories as $category) {
		$output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator;
	}
echo trim($output, $separator);
}


?></div>

<div class="author">
<?php if ( function_exists( 'coauthors_posts_links' ) ) {
    coauthors_posts_links();
} else {
    the_author_posts_link();
} ?>

</div>
</div><!--end post header-->
<div class="entry clear">
<?php if ( function_exists( 'add_theme_support' ) ) the_post_thumbnail(); ?>
<?php the_content(); ?>
<?php edit_post_link(); ?>
<?php wp_link_pages(); ?>
</div><!--end entry-->
<div class="post-footer">
<div class="comments"><?php comments_popup_link( 'Leave a Comment', '1 Comment', '% Comments' ); ?>
</div>
</div><!--end post footer-->
</div><!--end post-->
<?php endwhile; /* rewind or continue if all posts have been fetched */ ?>
<div class="navigation index">
<div class="alignleft"><?php next_posts_link( 'Older Entries' ); ?>
</div>
<div class="alignright"><?php previous_posts_link( 'Newer Entries' ); ?>
</div>
</div><!--end navigation-->
<?php else : ?>
<?php endif; ?>
</div>

</div> <!--end container-->
</div> <!--end container-->
       
</div> <!--end container-->
</div> <!--end container-->
<?php get_footer(); ?>
