<?php

//bootstrap navwalker plugin
require_once('wp_bootstrap_navwalker.php'); 

add_theme_support( 'post-thumbnails', array( 'post', 'page', 'article', 'presentation', 'overview_page' ) );

//advanced search plugin
require_once('wp-advanced-search/wpas.php');


/**
 * Proper way to enqueue scripts and styles
 */
add_filter('widget_text','do_shortcode');
add_action('init', 'cptui_register_my_cpt_cv');
function cptui_register_my_cpt_cv() {
register_post_type('cv', array(
'label' => 'CVs',
'description' => 'Curriculum Vitae page',
'public' => true,
'show_ui' => true,
'show_in_menu' => true,
'capability_type' => 'page',
'map_meta_cap' => true,
'hierarchical' => true,
'rewrite' => array('slug' => 'cv', 'with_front' => true),
'query_var' => true,
'has_archive' => true,
'supports' => array('title','editor','excerpt','trackbacks','custom-fields','revisions','thumbnail','author','page-attributes','post-formats'),
'labels' => array (
  'name' => 'CVs',
  'singular_name' => 'CV',
  'menu_name' => 'CVs',
  'add_new' => 'Add CV',
  'add_new_item' => 'Add New CV',
  'edit' => 'Edit',
  'edit_item' => 'Edit CV',
  'new_item' => 'New CV',
  'view' => 'View CV',
  'view_item' => 'View CV',
  'search_items' => 'Search CVs',
  'not_found' => 'No CVs Found',
  'not_found_in_trash' => 'No CVs Found in Trash',
  'parent' => 'Parent CV',
)
) ); }

function gun_scripts() {
	//wp_enqueue_style( 'style-name', get_stylesheet_uri() );
		wp_enqueue_script( 'Bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '1.0.0', true );
		wp_enqueue_script( 'Bootstrap Select', get_template_directory_uri() . '/js/bootstrap-select.min.js', array(), '1.0.0', true );
		wp_enqueue_script( 'Offcanvas', get_template_directory_uri() . '/js/offcanvas.js', array(), '1.0.0', true );
		//wp_enqueue_script( 'smoothscroll', get_template_directory_uri() . '/js/smoothscroll.js', array(), false, true);
		//wp_enqueue_script( 'smoothscroll', get_template_directory_uri() . '/js/smoothscroll.js', array(), '1.0.0', true );
}

add_action( 'wp_enqueue_scripts', 'gun_scripts' );

add_image_size( 'header',1698, 449, true ); 

//only load smoothscroll on Practice Areas (As there are no collapse elements to interact with)
function smoothscroll_conditional_loading() {
    if ( is_page_template( 'practice-areas.php' ) || is_page( 'practice-areas') || is_page_template('team.php')  ) {
                wp_enqueue_script( 'smoothscroll', get_template_directory_uri() . '/js/smoothscroll.js', array(), false, true);
    } 
}
add_action('wp_enqueue_scripts', 'smoothscroll_conditional_loading');



function my_search_args() {
 $home =  icl_get_home_url();
 $searchText2 = __('Ara','gunandpartners');
 $nullPractice = __('Hizmetlerimiz   ','gunandpartners');
 $nullInsights = __('Tüm Gelişmeler','gunandpartners');
 $nullDates = __('Tüm Tarihler','gunandpartners');
 $searchbutton = __('Arama','gunandpartners');
 $browseCategory = __("Arama Sınıfları","gunandpartners");
       		$args = array();
                $args['relevanssi'] = true;
                        $args['form'] = array('action' => $home.'search-results/',
                                                        'method' => 'GET',
                                                        'id' => 'wp-advanced-search',
                                                        'name' => 'wp-advanced-search',
                                                        'class' => 'wp-advanced-search');

			$args['wp_query'] = array('post_type' => 'post',
			                                'posts_per_page' => 6,
			                                'order' => 'DESC',
			                                'orderby' => 'date');

			$args['fields'][] = array('type' => 'search',
			                                'label' => '',
							'class' => 'form-control', 
							'placeholder' => $searchText2,
			                                'value' => $searchText2);
	  $field_cat = array();
    $categories =  get_categories('child_of='.icl_object_id(165, 'category', false).'&hide_empty=0');
    foreach ($categories as $key => $value) {
      $field_cat[] = $value->slug;
    }

			$args['fields'][] = array('type' => 'taxonomy',
				                                'label' =>  $browseCategory,
				                                'taxonomy' => 'category',
                                        'terms' => $field_cat,
                                        'nested' => true,
				                                'format' => 'select',
								'class' => 'selectpicker',
                                                                "allow_null" => $nullPractice,
				                                'operator' => 'AND');

			$args['fields'][] = array('type' => 'taxonomy',
				                                'label' => '',
				                                'taxonomy' => 'post_tag',
				                                'format' => 'select',
								'class' => 'selectpicker',
                                                                "allow_null" => $nullInsights,
				                                'operator' => 'IN');

			$args['fields'][] = array('type' => 'date',
			                                'label' => '',
			                                'date_type' => 'year',
			                                'format' => 'select',
                                                         "allow_null" => $nullDates,
							'class' => 'selectpicker');
				
				
			$args['fields'][] = array('type' => 'submit',
			                                'value' => $searchbutton,
							'class' => 'btn btn-primary');
 
      return $args;
 //add_action( 'init', 'my_add_excerpts_to_pages' );

}

function cv_search_args() {
$field_key = "field_53a1b76decdfe";
$field = get_field_object($field_key);
		$searchText = translate('Name, Role or Practice Group','gunandpartners');
       		$args2 = array();
                $home =  icl_get_home_url();
		$args2['relevanssi'] = true;
                $args2['form'] = array(         'action' => $home.'lawyer-search/',
                                                        'method' => 'GET',
                                                        'id' => 'wp-advanced-search',
                                                        'name' => 'wp-advanced-search',
                                                        'class' => 'wp-advanced-search right-inner-addon');

		$args2['fields'][] = array(     'type' => 'html',							
			                        'value' => '<span class="glyphicon glyphicon-search" style="color: #999999;"></span>');
		
		$args2['wp_query'] = array(     'post_type' => 'post',
			                                'posts_per_page' => 5,
                                      'category' => 'makaleler',
			                                'order' => 'DESC',
			                                'orderby' => 'date');

		
							
		$args2['fields'][] = array(     'type' => 'search',
			                                //'label' => '',
							'class' => 'form-control', 
							//'placeholder' => 'Name, Role or Practice Group',
			                                'placeholder' => $searchText,
							'value' => 'Search Term');
                       
//                       $args2['fields'][] = array(              'type' => 'meta_key',
//				                                'label' => '',
//                                                               // 'post_type' => 'cv',
//				                                'meta_key' => $field_key,
//				                                'format' => 'select',
//								'class' => 'selectpicker',
//                                                                'values' => $field['choices'],
//                                                                "allow_null" => "All Core Practice Groups",
//				                                'operator' => 'IN');
                        

			//$args2['fields'][] = array(     'type' => 'submit',
			                                //'value' => '<span class=\'glyphicon glyphicon-search\'></span>',
							  //'placeholder' => '<span class="glyphicon glyphicon-star"></span> Star',
							  
							//'class' => 'btn btn-default');
 foreach( $field['choices'] as $k => $v )
		{
			//echo '' . $k . '-' . $v . '<br/>';
		}
      return $args2;


}
add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}

//Language Switcher

function icl_post_languages(){
  $languages = icl_get_languages('skip_missing=1');
  if(1 < count($languages)){
    echo __('<span class="glyphicon glyphicon-globe" style="color: #999999;"></span> ');
    foreach($languages as $l){
      if(!$l['active'])
      { $langs[] = '<a href="'.$l['url'].'">'.$l['translated_name'].'</a>';
      }
      else {
        $langs[] = ''.$l['translated_name'].'';
      }
    }
    echo join(', ', $langs);
    echo '<hr>';
  }
}

//Language switcher for articles
function icl_post_languages_article(){
  $languages = icl_get_languages('skip_missing=1');
  if(1 < count($languages)){
    echo __('Other Languages: ');
    foreach($languages as $l){
      if(!$l['active']) $langs[] = '<a href="'.$l['url'].'">'.$l['translated_name'].'</a>';
    }
    echo join(', ', $langs);
    echo '<br/><br/><hr>';
  }
}

//Language Switcher Header

function icl_post_languages_header(){
  $languages = icl_get_languages('skip_missing=1');
  if(1 < count($languages)){
    echo __('This post is also available in: ');
    foreach($languages as $l){
      if(!$l['active']) $langs[] = '<a href="'.$l['url'].'">'.$l['translated_name'].'</a>';
    }
    echo join(', ', $langs);
  }
}


// numbered pagination
function pagination($pages = '', $range = 4)
{  
     $showitems = ($range * 2)+1;  
 
     global $paged;
     if(empty($paged)) $paged = 1;
 
     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   
 
     if(1 != $pages)
     {
         echo "<div class=\"pagination\"><span>Page ".$paged." of ".$pages."</span>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; </a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; </a>";
 
         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
             }
         }
 
         if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\"> &rsaquo;</a>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'> &raquo;</a>";
         echo "</div>\n";
     }
}

//REGISTER MENUS

function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' ),
      'extra-menu' => __( 'Extra Menu' ),
      'footer-menu' => __( 'Footer Menu' )
    )
  );
}
add_action( 'init', 'register_my_menus' );

/*RELEVANASSI HACKS FOR LAWYER SEARCH*/

add_filter('relevanssi_admin_search_ok', 'relevanssi_disable_filter_admin', 10, 2);
function relevanssi_disable_filter_admin($ok, $query) {
    if (empty($query->query_vars['s']) || $query->query_vars['s'] == " ") {
        return false;
    }
    else {
        return $ok;
    }
}
 
add_filter('relevanssi_search_ok', 'relevanssi_disable_filter');
function relevanssi_disable_filter($ok) {
    global $wp_query;
    if (empty($wp_query->query_vars['s']) || $wp_query->query_vars['s'] == " ") {
        return false;
    }
    else {
        return $ok;
    }
}
/**
 * ====================================================
 * Help Contact Form 7 Play Nice With Twitter Bootstrap
 * ==================================================== 
 * Add a Twitter Bootstrap-friendly class to the "Contact Form 7" form
 */
add_filter( 'wpcf7_form_class_attr', 'wildli_custom_form_class_attr' );
function wildli_custom_form_class_attr( $class ) {
	$class .= ' form-horizontal';
	return $class;
}


if ( function_exists('register_sidebar') )
register_sidebar(array(
'name' => 'Left Sidebar',
'before_widget' => '<div class="sidebar-link">',
'after_widget' => '</div>',
'before_title' => '<h5>',
'after_title' => '</h5>',
));
if ( function_exists('register_sidebar') )
register_sidebar(array(
'name' => 'Right Sidebar',
'before_widget' => '',
'after_widget' => '',
'before_title' => '<h2>',
'after_title' => '</h2>',
));





function the_content_by_id( $post_id=0, $more_link_text = null, $stripteaser = false ){
    global $post;
    $post = &get_post($post_id);
    setup_postdata( $post, $more_link_text, $stripteaser );
    the_content();
    wp_reset_postdata( $post );
}
?>