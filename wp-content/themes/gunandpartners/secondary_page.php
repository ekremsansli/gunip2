<?php
/**
/*
Template Name: Secondary Page
 * @package WordPress
 * @subpackage Gun + Partners
 * @since Gun + Partners 0.1
 */

get_header(); ?>
<script type="text/javascript">
	function toggleChevron(e) {
		window.alert("I am an alert box!");
    $(e.target)
        .prev('.panel-heading')
        .find("i.indicator")
        .toggleClass('glyphicon-minus glyphicon-plus');
	
}
$('#custom-collapse-0').on('hidden.bs.collapse', toggleChevron);
$('#custom-collapse-0').on('shown.bs.collapse', toggleChevron);
</script>
<?php get_sidebar(); ?>

<div id="main-content" class="main-content">
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post(); ?>

			<div class="row">
				<div class="col-xs-12"> <!--Page Title-->
					<h1 class="pageTitle"><?php the_title(); ?></h1>
				</div> <!--page title-->
				<?php 
if ( has_post_thumbnail() ) : ?>

<?php  // check if the post has a Post Thumbnail assigned to it.
        $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'header' );
  //the_post_thumbnail('header', array('class' => 'img-responsive scale'));
 
?>	
	<div class="col-12 col-sm-12 col-lg-12 col-xs-12 headerImage" style="
	background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(0,0,0,0) 59%, rgba(0,0,0,0.65) 100%), url(<?php echo $thumb['0'];?>) no-repeat center center; 
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(59%,rgba(0,0,0,0)), color-stop(100%,rgba(0,0,0,0.65))), url(<?php echo $thumb['0'];?>) no-repeat center center; 
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 59%,rgba(0,0,0,0.65) 100%), url(<?php echo $thumb['0'];?>) no-repeat center center; 
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 59%,rgba(0,0,0,0.65) 100%), url(<?php echo $thumb['0'];?>) no-repeat center center;  
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 59%,rgba(0,0,0,0.65) 100%), url(<?php echo $thumb['0'];?>) no-repeat center center; 
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 59%,rgba(0,0,0,0.65) 100%), url(<?php echo $thumb['0'];?>) no-repeat center center; 
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#a6000000',GradientType=0 ), url(<?php echo $thumb['0'];?>') no-repeat center center; 
  -webkit-background-size: 30px 44px, cover, cover;
  -moz-background-size: 30px 44px, cover, cover;
  -o-background-size: 30px 44px, cover, cover;
  background-size: 30px 44px, cover, cover;
  ">

	</div>
	    <?php endif ?>
			</div> <!--row-->
			<div class="row">
				<div class=" col-sm-12 col-md-9">
					<?php the_content(); ?>
				</div>	
			<?php endwhile;	?>
			</div> <!--row-->
		</div><!-- #content -->
	</div><!-- #primary -->
	
</div><!-- #main-content -->



<?php

get_footer(); ?>
