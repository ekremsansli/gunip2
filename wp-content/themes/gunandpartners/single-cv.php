<?php
/**
/*
Template Name: Single CV
 * @package WordPress
 * @subpackage Gun + Partners
 * @since Gun + Partners 0.1
 */

get_header(); ?>
<?php get_sidebar(); ?>

<div id="main-content" class="main-content">

	<div id="primary" class="content-area">
		<div id="content" class="site-content profile" role="main">

			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post(); ?>
<div class="row">
          <div class="col-lg-12 hidden-xs">

            
              <br/><br/>
          </div>
          
</div>
<div class="row">
	<!--HIDDEN PIC & CARD-->
	 <hr class="visible-xs">
	      <div class="col-6 col-sm-6 col-lg-6 col-xs-12 visible-xs">
 <?php 
 
$image = get_field('image');
 
if( !empty($image) ): ?>
 
	<img src="<?php echo $image['url']; ?>" alt="<?php the_field('name'); ?>" class="img-responsive"/>
 
<?php endif; ?>
    <br/>
<?php
	
	$vcard = get_field('vcard');
  
  if( !empty($vcard) ):  ?>
  <a href="<?php the_field("vcard");?>"><button type="button" class="btn btn-default">
  <span class="glyphicon glyphicon-user small" style="color: #00ABDF;"></span> Download vCard
</button></a>
  <?php endif ?>
  <?php
  
  $pdf = get_field('pdf');
  
  if( !empty($pdf) ): ?>
  <?php $file = get_field('pdf');
 
$url = wp_get_attachment_url( $file );

// part where to get the filesize
$filesize = filesize( get_attached_file( $file ) );
$filesize = size_format($filesize, 0);
//$filesize = number_format($filesize / 1048576, 4) . ' MB';
?>
    <a href="<?php echo $url;?>"><button type="button" class="btn btn-default" style="float: right;"  data-toggle="tooltip" data-placement="bottom" container=".single-cv" title="PDF - <?php echo $filesize; ?>" >
  <span class="glyphicon glyphicon-file" style="color: #00ABDF;"></span> CV 
</button></a>

    <?php endif ?>
    
    <hr>
  </div>
	<!--HIDDEN PIC & CARD-->
	
	<!--TITLE CARD-->
	<div class="col-5 col-sm-5 col-lg-5 col-xs-12 top cv">
		<h1 lang="<?=ICL_LANGUAGE_CODE?>"><?php the_title(); ?></h1>
		<h2 lang="<?=ICL_LANGUAGE_CODE?>"><?php
								$new_title = get_field('extra_title');
								//echo $new_title;
								if ($new_title)
								{
									echo $new_title;
								}
								else
								{
									$field = get_field_object('role');
									$value = get_field('role');
									$label = $field['choices'][ $value ];
									echo $label;
								}
								?></h2><br/>
		<address>
		<p>
		  <span class="phone"><?php the_field('telephone_number'); ?></span><br/>
		  <a href="mailto:<?php the_field('email_address'); ?>"><?php the_field('email_address'); ?></a>
		</p>
		<div class="visible-lg"><br/><br/></div>
		</address>
	<div class="bottom">
	      <?php if(get_field('member')): ?>
	      
	 <p><span><strong> <?php _e('Üyelikler','gunandpartners'); ?>:</strong></span><br/>
	<?php the_field("member");?>
	
	
	<?php endif?> <br/><br/>
      
      <span style="font-weight: 400; color: #00ABDF;"><strong><?php _e('Diller','gunandpartners'); ?>:</strong></span><br/>
      <?php the_field("languages");?><br/><br/>
      
      
      </p></div>
</div>
<div class="col-7 col-sm-7 col-lg-7 col-xs-7 hidden-xs">

<?php 
 
$image = get_field('image');
 
if( !empty($image) ): ?>
 
	<img src="<?php echo $image['url']; ?>" alt="<?php the_field('name'); ?>" class="img-responsive"/>
 
<?php endif; ?>

<!--<img src="img/mehmet_2.jpg" class="img-responsive">-->
	<?php
	
	$vcard = get_field('vcard');
  
  if( !empty($vcard) ):  ?>
  <a href="<?php the_field("vcard");?>"><button type="button" class="btn btn-default" style="float: right; border: none;">
  <span class="glyphicon glyphicon-user small" style="color: #00ABDF;"></span> Download vCard
</button></a>
  <?php endif ?>
  <?php
  
  $pdf = get_field('pdf');
  
  if( !empty($pdf) ): ?>
  <?php $file = get_field('pdf');
 
$url = wp_get_attachment_url( $file );

// part where to get the filesize
$filesize = filesize( get_attached_file( $file ) );
$filesize = size_format($filesize, 0);
//$filesize = number_format($filesize / 1048576, 4) . ' MB';
?>
    <a href="<?php echo $url;?>"><button type="button" class="btn btn-default" style="float: right;"  data-toggle="tooltip" data-placement="bottom" container=".single-cv" title="PDF - <?php echo $filesize; ?>" >
  <span class="glyphicon glyphicon-file" style="color: #00ABDF;"></span> <?php the_title();?>'s CV 
</button></a>

    <?php endif ?>
</div>             


</div>


<div class="row">
             <div class="col-10 col-sm-12 col-md-10 col-lg-10 col-xs-12">
              <h3 style="text-transform: none; color: #00ABDF; font-weight: 400; border-bottom: 1px solid #ddd; padding-bottom: 7px;"><?php _e('Ana Çalışma Sahalarımız','gunandpartners'); ?></h3>
<ul class="bio list-unstyled">
	
<?php	
	$cats = get_field('core_practice_areas');
echo '<ul class="bio list-unstyled">';
foreach ($cats as $key => $val){
    echo '<li>'.$val.'</li>';
}
echo '</ul>';
?>
	      <?php //echo the_field('core_practice_areas');?>
</div><!--/span-->
</div><!--/row-->
	  
	  
	  <div class="row">
  <?php
  $articles = get_field('extra_articles');
  if(!empty($articles)) : ?>
            <div class="col-10 col-sm-12 col-md-10 col-lg-10 col-xs-12">
		

              <h3><?php _e('Makaleler ve Yayınlar','gunandpartners'); ?></h3>
	      
	      <?php the_field('extra_articles'); ?>

	      
	      <p><?php echo the_field('article_author', 'user_$userID'); ?></p>
		<?php /*$authors = get_users('role=author');*/
		$author = get_the_author();
		//echo $author;

?>
<p><?php the_field('article_author', 'user_0'); ?></p>
<ul class="list-unstyled bio articles">
<?php



$author_id = get_the_author_meta( 'ID' );
$author_badge = get_field('ID', 'user_'. $author_id ); // image field, return type = "Image Object"

echo $author_badge;

$query = new WP_Query( 'author='.get_the_author_meta( 'ID' ).'' );

//$args = array( 'posts_per_page' => 5, 'orderby' => 'rand' ,'author' => '3');

//$curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $_GET['author_name']) : get_userdata($_GET['author']);
$curauth = get_user_by( 'id', get_queried_object()->post_author ); // get the info about the current author 


$args = array(
    'post_type' => 'post',
    'posts_per_page' => 10,
    'post_status' => 'publish',
//    'tax_query' => array(
//			array(
//				'taxonomy' => 'author',
//				'field' => 'slug',
//				'terms' =>  $curauth->user_login
//			)
//		)
    'author_name' => $curauth->user_login,
);
//echo $user_login;
$author_query = new WP_Query( $args );
 
if ( $author_query->have_posts() ) :
    while ( $author_query->have_posts() ) : $author_query->the_post();
 
    echo '<li><strong>';
    the_title();
    echo '</strong><br/>';

    $custom_excerpt = get_field('custom_excerpt');
    if(!$custom_excerpt)
    {
  //the_advanced_excerpt('length=20&length_type=words&no_custom=1&finish=sentence&exclude_tags=img,p,strong,br,h1,h2,h3,h4,h5&add_link=0');
    }
    else
    {
    	echo $custom_excerpt;
    }
    echo ' ';
    the_date("d m, Y", "", "");
    
	echo ' <a href="';
		the_permalink();
	echo '"><span class="glyphicon glyphicon-new-window small"></span></a></li>';
    
 
    endwhile;
endif;

?>


</ul>
            </div><!--/span-->
            
            <?php endif;
	    wp_reset_postdata();?>

          </div><!--/row-->
	  
				
				        
  <div class="row">
    <div class="col-10 col-sm-12 col-lg-10 col-xs-12">
              <h3><?php _e('Eğitim','gunandpartners'); ?></h3>
<ul class="list-unstyled bio articles">
<?php the_field('education'); ?>
</ul>
            </div><!--/span-->
  </div><br><br></div>

        </div><!--/span-->  </div><!--/row-->
				
					<!--the_content();

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) {
						comments_template();
					}-->
					
				<?php endwhile;
			?>



<?php

get_footer(); ?>
