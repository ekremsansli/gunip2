<?php
/**
/*
Template Name: Team
 * @package WordPress
 * @subpackage Gun + Partners
 * @since Gun + Partners 0.1
 */

get_header(); ?>
<?php get_sidebar(); ?>

<div id="main-content" class="main-content">

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post(); ?>

				<div class="row">
          <div class="col-6 col-sm-6 col-lg-6 col-xs-12">
            <h1 class="pageTitle" lang="<?=ICL_LANGUAGE_CODE?>"><?php the_title(); ?></h1>
            </div>
             
<?php 
if ( has_post_thumbnail() ) : ?>

<?php  // check if the post has a Post Thumbnail assigned to it.
        $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'header' );
  //the_post_thumbnail('header', array('class' => 'img-responsive scale'));
 
?>	
	<div class="col-12 col-sm-12 col-lg-12 col-xs-12 headerImage" style="
	background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(0,0,0,0) 59%, rgba(0,0,0,0.65) 100%), url(<?php echo $thumb['0'];?>) no-repeat center center; 
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(59%,rgba(0,0,0,0)), color-stop(100%,rgba(0,0,0,0.65))), url(<?php echo $thumb['0'];?>) no-repeat center center; 
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 59%,rgba(0,0,0,0.65) 100%), url(<?php echo $thumb['0'];?>) no-repeat center center; 
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 59%,rgba(0,0,0,0.65) 100%), url(<?php echo $thumb['0'];?>) no-repeat center center;  
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 59%,rgba(0,0,0,0.65) 100%), url(<?php echo $thumb['0'];?>) no-repeat center center; 
background: url('<?php echo get_template_directory_uri(); ?>/img/crosses_white_half.png') repeat-x bottom left, linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0) 59%,rgba(0,0,0,0.65) 100%), url(<?php echo $thumb['0'];?>) no-repeat center center; 
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#a6000000',GradientType=0 ), url(<?php echo $thumb['0'];?>') no-repeat center center; 
  -webkit-background-size: 30px 44px, cover, cover;
  -moz-background-size: 30px 44px, cover, cover;
  -o-background-size: 30px 44px, cover, cover;
  background-size: 30px 44px, cover, cover;
  ">

	</div>
	    <?php endif ?>
</div>
				
				  <div class="row">
  
             <div class="col-10 col-sm-12 col-md-10 col-lg-10 col-xs-12">
<?php the_content(); ?>

            <section id="partners">
            <?php
				$args = array( 'post_type' => 'cv', 'posts_per_page' => -1);
				$loop = new WP_Query( $args );
				$iter = 1; //iterator to insert clearfixes
			?>
			<?php while ( $loop->have_posts() ) : $loop->the_post();?>
			<div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xs-12 homeBox">
						<a href="<?php the_permalink() ?>"><span></span></a>
						<div class="row">
							<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xs-6">
								<figure>	
									<?php 	$image = get_field('image');
										if( !empty($image) ): ?>
											<img src="<?php echo $image['url']; ?>" alt="<?php the_field('name'); ?>" class="img-responsive"/>
										<?php else : ?>
											<img src="<?php echo get_template_directory_uri(); ?>/img/default_pic.jpg" alt="<?php the_field('name'); ?>" class="img-responsive"/>
									<?php endif; ?>
								</figure>
							</div>
							<div class="col-sm-12 col-xs-6">										 
								<h2 lang="<?=ICL_LANGUAGE_CODE?>"><?php the_title(); ?></h2>
								<h3 lang="<?=ICL_LANGUAGE_CODE?>">
									<?php
										$field = get_field_object('role');
										$value = get_field('role');
										$label = $field['choices'][ $value ];
										echo $label;
									?>
								</h3><br>
								<address>
									<p class="hidden-xs">
										<?php the_field('telephone_number'); ?><br/>
										<a href=""><?php the_field('email_address'); ?></a>
									</p>
								</address>
							</div>
						</div>
					</div>
					<?php if( $iter % 3 == 0 ):?>
			<div class="clearfix visible-xs-block"></div>
			<?php endif;
			$iter++;
			?>
			<?php endwhile; ?>
			<?php wp_reset_postdata(); ?>
									
			 
						 
											</section>
            </div><!--/span-->
           <div class="col-2 col-sm-12 col-md-2 col-lg-2 col-xs-12 right_menu" style="padding-top: 25px;">
               <!-- <div data-spy="affix">--><!--data-offset-top="60" data-offset-bottom="0"--> 
                <h4 lang="<?=ICL_LANGUAGE_CODE?>"><?php _e('Hızlı Erişim','gunandpartners') ?></h4>
		<ul class="nav sidebar">
					<li><a href="<?php echo get_permalink(icl_object_id(1008, 'page', true)); ?>"><?php echo get_the_title(icl_object_id(1008, 'page', true)); ?></a></li>
                  <li><a href="<?php echo get_permalink(icl_object_id(1014, 'page', true)); ?>"><?php echo get_the_title(icl_object_id(1014, 'page', true)); ?></a></li>
                  <li><a href="<?php echo get_permalink(icl_object_id(1018, 'page', true)); ?>"><?php echo get_the_title(icl_object_id(1018, 'page', true)); ?></a></li>
                  <li><a href="<?php echo get_permalink(icl_object_id(1022, 'page', true)); ?>"><?php echo get_the_title(icl_object_id(1022, 'page', true)); ?></a></li>
                  </li>
                 
                </ul>
        
           <!-- </div> -->
          </div><!--/row-->

        </div><!--/span-->
				
					<!--the_content();

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) {
						comments_template();
					}-->
					
				<?php endwhile;
			?>

		</div><!-- #content -->
	</div><!-- #primary -->
	
</div><!-- #main-content -->


<?php

get_footer(); ?>
