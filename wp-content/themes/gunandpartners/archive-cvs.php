<?php
$teamLink = get_permalink(icl_object_id(12,'page',false)); 
header("Location: $teamLink",TRUE,301);
?>
<?php
/**
/*
Template Name: Archive CV Page
 * @package WordPress
 * @subpackage Gun + Partners
 * @since Gun + Partners 0.1
 */

get_header(); ?>

<?php get_sidebar(); ?>

<div id="main-content" class="main-content">
	<div id="primary" class="content-area">
		<?php if ( have_posts() ) : ?>
		<div id="content" class="site-content" role="main">
<div class="row">
		<div class="col-xs-12">
            <h1 class="pageTitle"><?php single_cat_title(); ?></h1>
            <p> <?php single_cat_title(); ?> <?php _e('Insights: ','gunandpartners'); ?><?php echo  $wp_query->found_posts; ?></p></div> </div>
		
		<div class="row">
			<div class="col-xs-12"><hr></div></div>
		<div class="row">
			<div class="col-10 col-sm-12 col-lg-10 col-xs-12">
				<?php
				// Start the Loop.
				while ( have_posts() ) : the_post(); ?>
					<div class="row">
				<div class="col-xs-12">	
				<article>
					<div class="row">
						<!--THUMBNAIL-->
						<div class="col-xs-3 hidden-xs"> 
							<?php if ( has_post_thumbnail() ) : ?>
							<a href="<?php the_permalink() ?>"> <?php the_post_thumbnail('medium', array('class' => 'img-thumbnail')); ?></a>
							<?php else : ?>
							<a href="<?php the_permalink() ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/searchDefault.png" class="img-thumbnail" style="opacity: 0.75"></a>
							<?php endif ?>
						</div>
						 <!--END THUMBNAIL-->
						 <!--CONTENT-->
						<div class="col-sm-9 col-xs-12">
							<div class="row">
								<!--TITLE-->
								<div class="col-sm-9 col-xs-12">
									<h3 style="margin-bottom: 0px; margin-top: 0px;"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
								</div>
								<!--END TITLE-->
								<div class="col-sm-3 col-xs-12">
								<?php
									$pdf_file = get_field('pdf_version');
									if( !empty($pdf_file) ): ?>
									<?php $pdf_file = get_field('pdf_version');
										
									       $url = wp_get_attachment_url( $pdf_file );
									       $title = get_the_title( $pdf_file );
									       
									       // part where to get the filesize
									       $filesize = filesize( get_attached_file( $pdf_file ) );
									       $filesize = size_format($filesize, 0);
									       ?>
								<a href="<?php echo $url;?>">
									<button type="button" style="float:right; "class="btn btn-default"  data-toggle="tooltip" data-placement="bottom" title=".pdf - <?php echo $filesize; ?>" >
									<span class="glyphicon glyphicon-file" style="color: #00ABDF;"></span> PDF 
									</button>
								</a>

   <?php endif ?>
				</div>
				</div>
			
		
			
		
                <p><small><?php
$posttags = get_the_tags();
if ($posttags) {
  foreach($posttags as $tag) {
    echo $tag->name . ' - '; 
  }
}?><time datetime="<?php the_time( 'd.m.Y' ); ?>"><?php the_time( 'd.m.Y' ); ?></time><br/> <?php

$categories = get_the_category();
$separator = ' | ';
$output = '';
$i=0;
if($categories){
	foreach($categories as $category) {
		if ($i==2) break;
		$output .= ''.$category->cat_name.''.$separator;
		$i++;
	}
echo trim($output, $separator);
}


?> || <?php if ( function_exists( 'coauthors_posts_links' ) ) {
    coauthors();
} else {
    the_author_posts_link();
} ?></small></p>
		
                 <p class="byline"><?php the_advanced_excerpt('length=12&length_type=words&no_custom=1&finish=sentence&exclude_tags=img,p,strong,h1,h2,h3,h4,h5&read_more=%26#187;'); ?></p>

		
                                    </div></div><hr>
					</div></div>
               </article>
			<?php endwhile;	?>
			<?php else : ?>
			<div id="content" class="site-content" role="main">
				<div class="row">
					<div class="col-8 col-sm-8 col-lg-8 col-xs-12">
						<h1 class="pageTitle"><?php single_cat_title(); ?></h1>
						<p> <?php single_cat_title(); ?><?php _e('Insights: ','gunandpartners'); ?><?php echo  $wp_query->found_posts; ?></p></div> </div>
								
						<div class="row">
						<div class="col-xs-12"><hr></div>
						</div>
					</div>


				<?php endif;
			?>
			</div> <!--row--></div></div>
		</div><!-- #content -->
	</div><!-- #primary -->
	
</div><!-- #main-content -->



<?php

get_footer(); ?>
