<?php
/**
 * The template for displaying all posts
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Gun + Partners
 * @since Gun + Partners 0.1
 */

get_header(); ?>
<?php get_sidebar(); ?>

<div id="main-content" class="main-content">

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
<div class="row">
	<div class="col-12 col-sm-12 col-lg-12 col-xs-12"><img src="http://192.168.1.42:8888/gun/wp-content/uploads/2014/05/international_header.jpg" alt="" class="img-responsive"></div>
          <div class="col-8 col-sm-8 col-lg-8 col-xs-12">
		<?php
				// Start the Loop.
				while ( have_posts() ) : the_post(); ?>
            <h1 class="pageTitle"><?php the_title(); ?></h1>
	    <?php
  $posttags = get_the_tags();
  if ($posttags) {
    foreach($posttags as $tag) {
      echo $tag->name . ' '; 
    }
  }
?> &#8212; <?php the_date();?>
	  </div>
	   <div class="col-3 col-sm-3 col-lg-3 col-xs-12">
		<p><?php if ( function_exists( 'coauthors_posts_links' ) ) {
    coauthors();
} else {
    the_author_posts_link();
} ?> &nbsp;&nbsp; </p><p></p>
				<?php
				$categories = get_the_category();
				$separator = ' ';
				$output = '';
				if($categories){
					foreach($categories as $category) {
						$output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">'.$category->cat_name.'</a><br>'.$separator;
					}
				echo trim($output, $separator);
				}
	?> 
	    <?php
  
  $pdf = get_field('pdf_version');
  
  if( !empty($pdf) ): ?>
  <?php 
$url = wp_get_attachment_url( $pdf );
// part where to get the filesize
$filesize = filesize( get_attached_file( $pdf ) );
$filesize = size_format($filesize, 0);
?>
<a href="<?php echo $url;?>"><button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" container=".single-cv" title=".pdf - <?php echo $filesize; ?>" >
  <span class="glyphicon glyphicon-file" style="color: #00ABDF;"></span> Download PDF 
</button></a><?php endif ?>
 
	   
	   </div>
</div>
<hr>
<div class="row">
	<div class="col-8 col-sm-8 col-lg-8 col-xs-12">


					<?php the_content(); ?>

	  </div>
</div>


<?php endwhile; ?>
      <hr>
		<div class="row">
          <div class="col-9 col-sm-9 col-lg-9 col-xs-12">
									<h1>Header one</h1>
									<h2>Header two</h2>
									<h3>Header three</h3>
									<h4>Header four</h4>
									<h5>Header five</h5>
									<h6>Header six</h6>
									<p>Paragraph Test</p>


			
									<!-- Blockquotes -->
									<p>Single line blockquote:</p>
									<blockquote>Stay hungry. Stay foolish.</blockquote>
									<p>Multi line blockquote with a cite reference:</p>
									<blockquote>
									<p>People think focus means saying yes to the thing you've got to focus on. But that's not what it means at all. It means saying no to the hundred other good ideas that there are. You have to pick carefully. I'm actually as proud of the things we haven't done as the things I have done. Innovation is saying no to 1,000 things.</p>
									<footer><cite>Steve Jobs - Apple Worldwide Developers' Conference, 1997</cite></footer>
									</blockquote>
									<!-- Tables -->
									<table>
										<tbody>
											<tr>
												<th>Employee</th>
												<th class="views">Salary</th>
												<th></th>
											</tr>
											<tr class="odd">
												<td><a href="http://example.com/">Jane</a></td>
												<td>$1</td>
												<td>Because that's all Steve Job' needed for a salary.</td>
											</tr>
											<tr class="even">
												<td><a href="http://example.com/">John</a></td>
												<td>$100K</td>
												<td>For all the blogging he does.</td>
											</tr>
											<tr class="odd">
												<td><a href="http://example.com/">Jane</a></td>
												<td>$100M</td>
												<td>Pictures are worth a thousand words, right? So Tom x 1,000.</td>
											</tr>
											<tr class="even">
												<td><a href="http://example.com/">Jane</a></td>
												<td>$100B</td>
												<td>With hair like that?! Enough said...</td>
											</tr>
										</tbody>
									</table>
									<!-- Definition Lists -->
									<dl>
										<dt>Definition List Title:</dt>
										<dd>Definition list division.</dd>
										<dt>Startup:</dt>
										<dd>A startup company or startup is a company or temporary organization designed to search for a repeatable and scalable business model.</dd>
										<dt>#dowork:</dt>
										<dd>Coined by Rob Dyrdek and his personal body guard Christopher "Big Black" Boykins, "Do Work" works as a self motivator, to motivating your friends.</dd>
										<dt>Do It Live:</dt>
										<dd>I'll let Bill O'Reilly will <a title="We'll Do It Live" href="https://www.youtube.com/watch?v=O_HyZ5aW76c">explain</a> this one.</dd>
									</dl>
									<!-- Unordered Lists (Nested) -->
									<ul>
										<li>List item one
											<ul>
												<li>List item one
													<ul>
														<li>List item one</li>
														<li>List item two</li>
														<li>List item three</li>
														<li>List item four</li>
													</ul>
												</li>
												<li>List item two</li>
												<li>List item three</li>
												<li>List item four</li>
											</ul>
										</li>
										<li>List item two</li>
										<li>List item three</li>
										<li>List item four</li>
									</ul>
									<!-- Ordered List (Nested) -->
									<ol>
										<li>List item one
											<ol>
												<li>List item one
													<ol>
														<li>List item one</li>
														<li>List item two</li>
														<li>List item three</li>
														<li>List item four</li>
													</ol>
												</li>
												<li>List item two</li>
												<li>List item three</li>
												<li>List item four</li>
											</ol>
										</li>
										<li>List item two</li>
										<li>List item three</li>
										<li>List item four</li>
									</ol>
									<!-- Ordered-Unordered-Ordered List -->
									<ol>
										<li>ordered item</li>
										<li>ordered item
											<ul>
												<li><strong>unordered</strong></li>
												<li><strong>unordered</strong>
													<ol>
														<li>ordered item</li>
														<li>ordered item</li>
													</ol>
												</li>
											</ul>
										</li>
										<li>ordered item</li>
										<li>ordered item</li>
									</ol>
									<!-- Ordered-Unordered-Unordered List -->
									<ol>
										<li>ordered item</li>
										<li>ordered item
											<ul>
												<li><strong>unordered</strong></li>
												<li><strong>unordered</strong>
													<ul>
														<li>unordered item</li>
														<li>unordered item</li>
													</ul>
												</li>
											</ul>
										</li>
										<li>ordered item</li>
										<li>ordered item</li>
									</ol>
									<!-- Unordered-Ordered-Unordered List -->
									<ul>
										<li>unordered item</li>
										<li>unordered item
											<ol>
												<li>ordered</li>
												<li>ordered
													<ul>
														<li>unordered item</li>
														<li>unordered item</li>
													</ul>
												</li>
											</ol>
										</li>
										<li>unordered item</li>
										<li>unordered item</li>
									</ul>
									<!-- Unordered-Unordered-Ordered List -->
									<ul>
										<li>unordered item</li>
										<li>unordered item
											<ul>
												<li>unordered</li>
												<li>unordered
													<ol>
														<li><strong>ordered item</strong></li>
														<li><strong>ordered item</strong></li>
													</ol>
												</li>
											</ul>
										</li>
										<li>unordered item</li>
										<li>unordered item</li>
									</ul>
									<!-- HTML Tags -->
									<strong>Address Tag</strong>
									<p><address>1 Infinite Loop Cupertino, CA 95014 United States</address> example text</p>
												
			
</div> </div>
		</div><!-- #content -->
	</div><!-- #primary -->
	
</div><!-- #main-content -->

<?php

get_footer(); ?>
