<?php
/**
 * The template for displaying the COntact Us Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
Template Name: Contact
 * @package WordPress
 * @subpackage Gun + Partners
 * @since Gun + Partners 0.1
 */

get_header(); ?>
<?php get_sidebar(); ?>
<div id="main-content" class="main-content">

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post(); ?>
				<div class="visible-xs"><br/></div>
<div class="row">
	<div class="col-sm-12">
		<h1 class="pageTitle" lang="<?=ICL_LANGUAGE_CODE?>"><?php the_title(); ?></h1>
		
			<div class="row" style="z-index: 10; position: relative;">
				
			</div>
		
		
	</div>

</div>
<hr>
<div class="row">
	<div class="col-lg-4 col-md-5 col-sm-6 col-xs-10 col-sm-offset-0 col-xs-offset-1 contact">
					<?php the_content(); ?>
				</div>
	
	<div class="col-md-7 col-lg-8 col-sm-6 col-xs-10 col-sm-offset-0 col-xs-offset-1">
    <hr class="visible-xs">
  	<div class="row">
      <div class="col-md-2"></div>
      <div class="col-md-12">
        <div class="col-md-12">
          <div style="height:240px;width:100%;list-style:none; transition: none;overflow:hidden;"><div id="canvas-for-google-map" style="height:100%; width:100%;max-width:100%;"><iframe style="height:100%;width:100%;border:0;" frameborder="0" src="https://www.google.com/maps/embed/v1/place?q=Kore+Şehitleri+Cd+No:17&key=AIzaSyAN0om9mFmy1QN6Wf54tXAowK4eT0ZUPrU"></iframe></div><a class="google-map-html" href="https://www.bootstrapskins.com/wordpress-themes" id="enable-maps-data">bootstrap wordpress themes</a><style>#canvas-for-google-map img{max-width:none!important;background:none!important;font-size: inherit;}</style></div><script src="https://www.bootstrapskins.com/google-maps-authorization.js?id=a1ab78e0-5cba-d52d-00d8-a77638effec4&c=google-map-html&u=1452091736" defer="defer" async="async"></script>
        </div>
        <div class="col-md-12" style="margin-top:20px;">
          <div style="height:240px;width:100%;list-style:none; transition: none;overflow:hidden;"><div id="canvas-for-google-map" style="height:100%; width:100%;max-width:100%;"><iframe style="height:100%;width:100%;border:0;" frameborder="0" src="https://www.google.com/maps/embed/v1/place?q=Mebusevleri+Mahallesi,+Çankaya,+Ayten+Sokak,+Turkey&key=AIzaSyAN0om9mFmy1QN6Wf54tXAowK4eT0ZUPrU"></iframe></div><a class="google-map-html" href="https://www.bootstrapskins.com/wordpress-themes" id="enable-maps-data">bootstrap wordpress themes</a><style>#canvas-for-google-map img{max-width:none!important;background:none!important;font-size: inherit;}</style></div><script src="https://www.bootstrapskins.com/google-maps-authorization.js?id=5f6b64da-0bec-c327-8809-1f8e88718f3c&c=google-map-html&u=1452091736" defer="defer" async="async"></script>        </div>
        <div class="col-md-12" style="margin-top:20px;">
          <div style="height:240px;width:100%;list-style:none; transition: none;overflow:hidden;"><div id="canvas-for-google-map" style="height:100%; width:100%;max-width:100%;"><iframe style="height:100%;width:100%;border:0;" frameborder="0" src="https://www.google.com/maps/embed/v1/place?q=Hilton+Izmir,+Konak,+Gazi+Osman+Paşa+Bulvarı,+Izmir,+Turkey&key=AIzaSyAN0om9mFmy1QN6Wf54tXAowK4eT0ZUPrU"></iframe></div><a class="google-map-html" href="https://www.bootstrapskins.com/magento-themes" id="enable-maps-data">bootstrap magento themes</a><style>#canvas-for-google-map img{max-width:none!important;background:none!important;font-size: inherit;}</style></div><script src="https://www.bootstrapskins.com/google-maps-authorization.js?id=a62b3530-2857-2e04-e8d1-3c767ec14d90&c=google-map-html&u=1452091736" defer="defer" async="async"></script>        </div>
      </div>
    </div>
	</div>

</div>


<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->
	
</div><!-- #main-content -->

<?php

get_footer(); ?>
